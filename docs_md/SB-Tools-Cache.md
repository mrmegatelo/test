SB\Tools\Cache
===============

Class Cache




* Class name: Cache
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $cache

    private mixed $cache = null





* Visibility: **private**


### $cacheTime

    private mixed $cacheTime = 60 * 60 * 60 * 24





* Visibility: **private**


### $useCache

    private mixed $useCache = true





* Visibility: **private**


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### __construct

    mixed SB\Tools\Cache::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### setTime

    mixed SB\Tools\Cache::setTime($value)





* Visibility: **public**


#### Arguments
* $value **mixed**



### initialize

    mixed SB\Tools\Cache::initialize(array $params, callable $calcFunction, array $calcFunctionParameters)





* Visibility: **public**


#### Arguments
* $params **array**
* $calcFunction **callable**
* $calcFunctionParameters **array**



### clear

    mixed SB\Tools\Cache::clear(array $params)





* Visibility: **public**


#### Arguments
* $params **array**



### isUseCache

    boolean SB\Tools\Cache::isUseCache()





* Visibility: **public**




### setUseCache

    mixed SB\Tools\Cache::setUseCache(boolean $useCache)





* Visibility: **public**


#### Arguments
* $useCache **boolean**



### getInstance

    static SB\Tools\Cache::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Tools\Cache::reset()





* Visibility: **public**
* This method is **static**.




### __wakeup

    mixed SB\Tools\Cache::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Tools\Cache::__clone()





* Visibility: **protected**



