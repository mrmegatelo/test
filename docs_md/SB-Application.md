SB\Application
===============

Class Application
Класс программы, для реализации паттерна &quot;Фасад&quot; в приложении
в \SB\Site создать наследника




* Class name: Application
* Namespace: SB
* This is an **abstract** class
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md)







