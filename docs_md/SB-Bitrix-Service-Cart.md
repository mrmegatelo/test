SB\Bitrix\Service\Cart
===============

Сервис: Работа с корзиной




* Class name: Cart
* Namespace: SB\Bitrix\Service
* Parent class: [SB\Bitrix\Service](SB-Bitrix-Service.md)





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getCartCount

    integer SB\Bitrix\Service\Cart::getCartCount(integer $customerId)

Получение количества товаров в корзине покупателя



* Visibility: **public**


#### Arguments
* $customerId **integer** - &lt;ul&gt;
&lt;li&gt;идентификатор покупателя&lt;/li&gt;
&lt;/ul&gt;



### getInstance

    static SB\Bitrix\Service\Cart::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Bitrix\Service\Cart::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Bitrix\Service::__construct()





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Service](SB-Bitrix-Service.md)




### __wakeup

    mixed SB\Bitrix\Service\Cart::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Bitrix\Service\Cart::__clone()





* Visibility: **protected**




### loadModules

    mixed SB\Bitrix\Service::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Service](SB-Bitrix-Service.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Service::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Service](SB-Bitrix-Service.md)



