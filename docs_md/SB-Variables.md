SB\Variables
===============

Class Variables
Базовый класс хранилища
Хранит общие переменные
Все переменные объявлять как protected
Чтобы PhpStorm подсказывал добавить PHPDoc property в описание класса
Для переменных которые нельзя изменять добавить PHPDoc property-read в описание класса




* Class name: Variables
* Namespace: SB
* This is an **abstract** class
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md)




Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getInstance

    static SB\Variables::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Variables::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Variables::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Variables::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Variables::__clone()





* Visibility: **protected**



