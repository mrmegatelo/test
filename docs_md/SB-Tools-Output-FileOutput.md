SB\Tools\Output\FileOutput
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: FileOutput
* Namespace: SB\Tools\Output
* Parent class: [SB\Tools\Output](SB-Tools-Output.md)





Properties
----------


### $fh

    protected mixed $fh





* Visibility: **protected**


### $filePath

    protected mixed $filePath





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Tools\Output\FileOutput::__construct($filePath)

Constructor

TODO: need to add creating directory

* Visibility: **public**


#### Arguments
* $filePath **mixed**



### write

    mixed SB\Tools\Output::write($text)





* Visibility: **public**
* This method is **abstract**.
* This method is defined by [SB\Tools\Output](SB-Tools-Output.md)


#### Arguments
* $text **mixed**



### writeln

    mixed SB\Tools\Output::writeln($text)





* Visibility: **public**
* This method is **abstract**.
* This method is defined by [SB\Tools\Output](SB-Tools-Output.md)


#### Arguments
* $text **mixed**



### __destruct

    mixed SB\Tools\Output\FileOutput::__destruct()





* Visibility: **public**



