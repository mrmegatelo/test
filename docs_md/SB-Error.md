SB\Error
===============

Class Error
Базовый класс ошибок
Описывает критические ошибка, внутренние ошибки PHP




* Class name: Error
* Namespace: SB
* Parent class: Error
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md), [SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)






Methods
-------


### __construct

    mixed SB\Error::__construct(string $message, integer $code, \SB\Interfaces\Throwable|null $previous)

Error constructor.



* Visibility: **public**


#### Arguments
* $message **string**
* $code **integer**
* $previous **[SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)|null**


