SB\Bitrix\Snippet\OrderType
===============

Class OrderType
Спипетты сортировки




* Class name: OrderType
* Namespace: SB\Bitrix\Snippet
* This is an **abstract** class
* Parent class: [SB\Snippet](SB-Snippet.md)



Constants
----------


### byIdDesc

    const byIdDesc = array('ID' => 'DESC')





### byIdAsc

    const byIdAsc = array('ID' => 'ASC')





### bySortDesc

    const bySortDesc = array('SORT' => 'DESC')





### bySortAsc

    const bySortAsc = array('SORT' => 'ASC')





### byShowsDesc

    const byShowsDesc = array('shows' => 'DESC')





### byShowsAsc

    const byShowsAsc = array('shows' => 'ASC')





### byNameDesc

    const byNameDesc = array('NAME' => 'DESC')





### byNameAsc

    const byNameAsc = array('NAME' => 'ASC')








