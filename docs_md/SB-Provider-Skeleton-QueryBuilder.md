SB\Provider\Skeleton\QueryBuilder
===============

Class QueryBuilder




* Class name: QueryBuilder
* Namespace: SB\Provider\Skeleton
* This is an **abstract** class





Properties
----------


### $configuration

    public \SB\Provider\Skeleton\Configuration $configuration = null





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Provider\Skeleton\QueryBuilder::__construct(\SB\Provider\Skeleton\Configuration $configuration)

QueryBuilder constructor.



* Visibility: **public**


#### Arguments
* $configuration **[SB\Provider\Skeleton\Configuration](SB-Provider-Skeleton-Configuration.md)**



### create

    mixed SB\Provider\Skeleton\QueryBuilder::create(array $arParams)





* Visibility: **public**
* This method is **abstract**.


#### Arguments
* $arParams **array**


