SB\Tools\Menu
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Menu
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### getTree

    array SB\Tools\Menu::getTree(array $arData, array $arKeys)

формирует вложенное дерево из стандартного результата компонента меню



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arData **array**
* $arKeys **array** - &lt;p&gt;массив ключей. Доступные значения lvl, children. Могут быть переназначены на нужные значения, по дефолту DEPTH_LEVEL, CHILDREN&lt;/p&gt;


