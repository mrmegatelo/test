SB\Tools\File
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: File
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### addLeftSlash

    string SB\Tools\File::addLeftSlash($path, boolean $argAsLink, string $slashChar)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $path **mixed**
* $argAsLink **boolean** - &lt;ul&gt;
&lt;li&gt;флаг что первый аргумент передается по ссылки&lt;/li&gt;
&lt;/ul&gt;
* $slashChar **string** - &lt;ul&gt;
&lt;li&gt;разделитель директорий&lt;/li&gt;
&lt;/ul&gt;



### addRightSlash

    string SB\Tools\File::addRightSlash($path, boolean $argAsLink, string $slashChar)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $path **mixed**
* $argAsLink **boolean** - &lt;ul&gt;
&lt;li&gt;флаг что первый аргумент передается по ссылки&lt;/li&gt;
&lt;/ul&gt;
* $slashChar **string** - &lt;ul&gt;
&lt;li&gt;разделитель директорий&lt;/li&gt;
&lt;/ul&gt;



### walkDir

    boolean SB\Tools\File::walkDir($dirPath, \Closure $callback, \Closure|null $callbackFilter)

Обходит дирректорию



* Visibility: **public**
* This method is **static**.


#### Arguments
* $dirPath **mixed** - &lt;ul&gt;
&lt;li&gt;полный путь до бирректории которую надо обойти&lt;/li&gt;
&lt;/ul&gt;
* $callback **Closure** - &lt;ul&gt;
&lt;li&gt;колбэк функция срабатывает на каждый файл и директорию&lt;/li&gt;
&lt;/ul&gt;
* $callbackFilter **Closure|null** - &lt;p&gt;($path, $isDir)  - колбэк функция срабатывает на каждый файл и директорию, используется для фильтрации&lt;/p&gt;



### dirSize

    mixed SB\Tools\File::dirSize($dirPath)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $dirPath **mixed**



### createDir

    mixed SB\Tools\File::createDir($path, $mode, $recursive)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $path **mixed**
* $mode **mixed**
* $recursive **mixed**


