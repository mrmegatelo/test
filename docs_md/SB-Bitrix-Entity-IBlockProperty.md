SB\Bitrix\Entity\IBlockProperty
===============

Сущность &quot;Свойства инфоблока&quot;
Содержит общие свойства и методы




* Class name: IBlockProperty
* Namespace: SB\Bitrix\Entity
* Parent class: [SB\Entity](SB-Entity.md)



Constants
----------


### arAdditionMessage

    const arAdditionMessage = array('CREATE_EXCEPTION' => 'Не удалось создать свойство', 'CREATE_EXCEPTION_IS_EXIST' => 'Свойство уже существует', 'GET_EXCEPTION' => 'Свойство не найдено')







Methods
-------


### createProperties

    array SB\Bitrix\Entity\IBlockProperty::createProperties($arProperties)

Создает несколько свойств



* Visibility: **public**


#### Arguments
* $arProperties **mixed**



### create

    boolean SB\Bitrix\Entity\IBlockProperty::create($arData)

Создает свойство



* Visibility: **protected**


#### Arguments
* $arData **mixed**



### isExist

    boolean SB\Bitrix\Entity\IBlockProperty::isExist($iBlockId, $propertyCode)

Проверяет существование свойства



* Visibility: **protected**


#### Arguments
* $iBlockId **mixed**
* $propertyCode **mixed**



### getPropertyEnum

    array SB\Bitrix\Entity\IBlockProperty::getPropertyEnum($iBlockID, $propertyCode)

Получает значения свойств типа список



* Visibility: **public**


#### Arguments
* $iBlockID **mixed**
* $propertyCode **mixed**


