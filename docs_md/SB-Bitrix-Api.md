SB\Bitrix\Api
===============

Class Api




* Class name: Api
* Namespace: SB\Bitrix
* This is an **abstract** class
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md)






Methods
-------


### __construct

    mixed SB\Bitrix\Api::__construct()





* Visibility: **public**




### loadModules

    mixed SB\Bitrix\Api::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Api::getSiteId()





* Visibility: **public**



