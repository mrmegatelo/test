SB\Response\Ajax
===============

Class Response
Базовый класс ответов




* Class name: Ajax
* Namespace: SB\Response
* Parent class: [SB\Response](SB-Response.md)





Properties
----------


### $responseModel

    protected mixed $responseModel = \SB\Model\Ajax\Response::class





* Visibility: **protected**


### $errorModel

    protected mixed $errorModel = \SB\Model\Ajax\Error::class





* Visibility: **protected**


### $response

    protected \SB\Model\Ajax\Response $response = null





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Response\Ajax::__construct($status, $data, $error)





* Visibility: **public**


#### Arguments
* $status **mixed**
* $data **mixed**
* $error **mixed**



### setStatus

    void SB\Response\Ajax::setStatus(boolean $status)





* Visibility: **public**


#### Arguments
* $status **boolean**



### addError

    void SB\Response\Ajax::addError(string $string, integer $code)





* Visibility: **public**


#### Arguments
* $string **string**
* $code **integer**



### addResult

    void SB\Response\Ajax::addResult(string $key, mixed $value)





* Visibility: **public**


#### Arguments
* $key **string**
* $value **mixed**



### getStatus

    boolean SB\Response\Ajax::getStatus()





* Visibility: **public**




### getData

    array SB\Response\Ajax::getData()





* Visibility: **public**




### getErrors

    array SB\Response\Ajax::getErrors()





* Visibility: **public**




### getAnswer

    \SB\Model\Ajax\Response SB\Response\Ajax::getAnswer()





* Visibility: **public**



