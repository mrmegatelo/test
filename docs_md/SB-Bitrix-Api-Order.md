SB\Bitrix\Api\Order
===============

Class Api




* Class name: Order
* Namespace: SB\Bitrix\Api
* Parent class: [SB\Bitrix\Api](SB-Bitrix-Api.md)







Methods
-------


### __construct

    mixed SB\Bitrix\Api::__construct()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)




### create

    mixed SB\Bitrix\Api\Order::create($arParams)





* Visibility: **public**


#### Arguments
* $arParams **mixed**



### getPropertyByCode

    \Bitrix\Sale\PropertyValue SB\Bitrix\Api\Order::getPropertyByCode($propertyCollection, $code)





* Visibility: **public**


#### Arguments
* $propertyCollection **mixed**
* $code **mixed**



### loadModules

    mixed SB\Bitrix\Api::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Api::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)



