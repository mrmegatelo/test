SB\Bitrix\Snippet\PropertyValues
===============

Class PropertyValues
Спиппеты для свойств




* Class name: PropertyValues
* Namespace: SB\Bitrix\Snippet
* Parent class: [SB\Snippet](SB-Snippet.md)



Constants
----------


### enumYesNo

    const enumYesNo = array(array('VALUE' => 'Да', 'DEF' => 'N', 'SORT' => '1'), array('VALUE' => 'Нет', 'DEF' => 'Y', 'SORT' => '2'))








