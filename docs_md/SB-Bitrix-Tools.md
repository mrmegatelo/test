SB\Bitrix\Tools
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Tools
* Namespace: SB\Bitrix
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### __construct

    mixed SB\Bitrix\Tools::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### loadModules

    mixed SB\Bitrix\Tools::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Tools::getSiteId()





* Visibility: **public**




### getInstance

    static SB\Bitrix\Tools::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Bitrix\Tools::reset()





* Visibility: **public**
* This method is **static**.




### __wakeup

    mixed SB\Bitrix\Tools::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Bitrix\Tools::__clone()





* Visibility: **protected**



