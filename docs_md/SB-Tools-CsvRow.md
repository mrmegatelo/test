SB\Tools\CsvRow
===============






* Class name: CsvRow
* Namespace: SB\Tools





Properties
----------


### $Csv

    public \SB\Tools\Csv $Csv = null





* Visibility: **public**


### $arCells

    private mixed $arCells = array()





* Visibility: **private**


Methods
-------


### __construct

    mixed SB\Tools\CsvRow::__construct($Csv)





* Visibility: **public**


#### Arguments
* $Csv **mixed**



### getCells

    mixed SB\Tools\CsvRow::getCells()





* Visibility: **public**




### setCells

    mixed SB\Tools\CsvRow::setCells($arCells)





* Visibility: **public**


#### Arguments
* $arCells **mixed**



### setCell

    mixed SB\Tools\CsvRow::setCell($columnIndex, $value)





* Visibility: **public**


#### Arguments
* $columnIndex **mixed**
* $value **mixed**



### addCell

    mixed SB\Tools\CsvRow::addCell($value)





* Visibility: **public**


#### Arguments
* $value **mixed**



### setCellByName

    mixed SB\Tools\CsvRow::setCellByName($column, $value)





* Visibility: **public**


#### Arguments
* $column **mixed**
* $value **mixed**



### getCell

    mixed SB\Tools\CsvRow::getCell($index)

Получает колонку по индексу



* Visibility: **public**


#### Arguments
* $index **mixed**



### getCellByName

    boolean SB\Tools\CsvRow::getCellByName($column)

Получает ячейку текущей строки по названию колонки



* Visibility: **public**


#### Arguments
* $column **mixed**


