SB\Error\Construct
===============

Class Construct




* Class name: Construct
* Namespace: SB\Error
* Parent class: [SB\Error](SB-Error.md)







Methods
-------


### __construct

    mixed SB\Error::__construct(string $message, integer $code, \SB\Interfaces\Throwable|null $previous)

Error constructor.



* Visibility: **public**
* This method is defined by [SB\Error](SB-Error.md)


#### Arguments
* $message **string**
* $code **integer**
* $previous **[SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)|null**


