SB\Tools\Common
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Common
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### extract_array

    array SB\Tools\Common::extract_array($aSrc, mixed $mKeys, mixed $bUseKeyExists, mixed $mFillValue)

Возвращает подмассив из $aSrc только с ключами $mKeys



* Visibility: **public**
* This method is **static**.


#### Arguments
* $aSrc **mixed**
* $mKeys **mixed** - &lt;ul&gt;
&lt;li&gt;массив либо строка с разделителем &quot;,&quot;&lt;/li&gt;
&lt;/ul&gt;
* $bUseKeyExists **mixed** - &lt;ul&gt;
&lt;li&gt;Для определения существования элемента будет использоваться функция array_key_exists.
требуется вслучие когда нужно сохранить значения null&lt;/li&gt;
&lt;/ul&gt;
* $mFillValue **mixed** - &lt;ul&gt;
&lt;li&gt;заполнять заначеними $mFillValue отсутствующие элемент в $aSrc&lt;/li&gt;
&lt;/ul&gt;



### replace_key_array

    mixed SB\Tools\Common::replace_key_array($aRows, $sKey)

Заменяет ключ в $aRows на значени элемента подмассива c ключем $sKey

$aResult[$aRows[i][$sKey]] = $aRows[i]

* Visibility: **public**
* This method is **static**.


#### Arguments
* $aRows **mixed**
* $sKey **mixed**



### getList

    array SB\Tools\Common::getList($aRows, $sKey, $sKeyValue)

Получает список. Ключ - значение элемента $aRows с ключем $sKey, Значение - значение элемента $aRows с ключем $sKeyValue



* Visibility: **public**
* This method is **static**.


#### Arguments
* $aRows **mixed**
* $sKey **mixed**
* $sKeyValue **mixed**



### getListInOrder

    array SB\Tools\Common::getListInOrder($aRows, $sKeyValue)

Получает список. Ключ - ключ из $aRows, Значение - значение элемента $aRows с ключем $sKeyValue



* Visibility: **public**
* This method is **static**.


#### Arguments
* $aRows **mixed**
* $sKeyValue **mixed**



### array_trim

    boolean SB\Tools\Common::array_trim(array $aRes, array $aKeys)

Делает трим для элементов одномерного массива



* Visibility: **public**
* This method is **static**.


#### Arguments
* $aRes **array**
* $aKeys **array**



### arrayMapOne

    null SB\Tools\Common::arrayMapOne($arSrc, $arMap, boolean $deleteNullValue)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $arSrc **mixed**
* $arMap **mixed**
* $deleteNullValue **boolean**



### arrayMap

    mixed SB\Tools\Common::arrayMap($arSrc, $arMap, $deleteNullValue)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $arSrc **mixed**
* $arMap **mixed**
* $deleteNullValue **mixed**



### deleteNullValue

    mixed SB\Tools\Common::deleteNullValue($arResult)

Рекурсивно Удаляет все значения null



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arResult **mixed**



### GetPagePath

    string SB\Tools\Common::GetPagePath(boolean $page, null $get_index_page, array $arArgs)

Формирует урл адрес с get-параметрами. без указания параметра $arArgs работает как GetPagePath()



* Visibility: **public**
* This method is **static**.


#### Arguments
* $page **boolean**
* $get_index_page **null**
* $arArgs **array** - &lt;ul&gt;
&lt;li&gt;массив параметров которые надо подставить, если элемент равен null параметр удаляется&lt;/li&gt;
&lt;/ul&gt;



### get

    mixed SB\Tools\Common::get($aArr, $mKey, $mReturn)

Получает элемент массива



* Visibility: **public**
* This method is **static**.


#### Arguments
* $aArr **mixed**
* $mKey **mixed**
* $mReturn **mixed**



### getAttrUrl

    null SB\Tools\Common::getAttrUrl(boolean $num)

Получает атрибуты из урл. часть пути разделенное слешем

site.ru/<атрибут 0>/<атрибут 1>/<атрибут 2>/.../

* Visibility: **public**
* This method is **static**.


#### Arguments
* $num **boolean**



### fastEncrypt

    string SB\Tools\Common::fastEncrypt($key, $text, $complate)

Шифрует по ключу, и переводит в base64



* Visibility: **public**
* This method is **static**.


#### Arguments
* $key **mixed** - &lt;ul&gt;
&lt;li&gt;ключ шифрования&lt;/li&gt;
&lt;/ul&gt;
* $text **mixed** - &lt;ul&gt;
&lt;li&gt;шифруемый текст&lt;/li&gt;
&lt;/ul&gt;
* $complate **mixed**



### fastDecrypt

    string SB\Tools\Common::fastDecrypt($key, $code, $complate)

Расшировывает данные зашифрованные функцией fastEncrypt



* Visibility: **public**
* This method is **static**.


#### Arguments
* $key **mixed** - &lt;ul&gt;
&lt;li&gt;ключ шифрования&lt;/li&gt;
&lt;/ul&gt;
* $code **mixed** - &lt;ul&gt;
&lt;li&gt;шифрованные данные&lt;/li&gt;
&lt;/ul&gt;
* $complate **mixed**



### getHostPart

    string SB\Tools\Common::getHostPart(mixed $position)

Получает часть хоста, поддомен



* Visibility: **public**
* This method is **static**.


#### Arguments
* $position **mixed** - &lt;p&gt;(0,1,2,3,...,-1,-2,...). 0 - зона(ru,com), -1 (самый последний поддомен например: krasnoyars.rabota.1cbit.ru функция вернет krasnoyarsk);&lt;/p&gt;



### array_clear

    mixed SB\Tools\Common::array_clear($arSrc, $value, boolean $strict)

Удаляет все элементы $value из массива $arSrc



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arSrc **mixed**
* $value **mixed**
* $strict **boolean** - &lt;ul&gt;
&lt;li&gt;строгое соответстиве&lt;/li&gt;
&lt;/ul&gt;



### convertToDec

    integer|mixed SB\Tools\Common::convertToDec($arAlphabet, $value)

Конвертирует из Системы счисления алфавита $arAlphabet в десятичную



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arAlphabet **mixed**
* $value **mixed**



### decToAlphabet

    string SB\Tools\Common::decToAlphabet($arAlphabet, $value)

Конвертирует из десятичную в систему счисления алфавита $arAlphabet



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arAlphabet **mixed**
* $value **mixed**



### renderTpl

    string SB\Tools\Common::renderTpl($filePath, array $arTplParams)

Выполняет инклуд файла $filePath, но не выводит на экран, а возвращает получившийся html



* Visibility: **public**
* This method is **static**.


#### Arguments
* $filePath **mixed** - &lt;ul&gt;
&lt;li&gt;Путь до файла&lt;/li&gt;
&lt;/ul&gt;
* $arTplParams **array** - &lt;ul&gt;
&lt;li&gt;массив переменных выводимых в шаблоне&lt;/li&gt;
&lt;/ul&gt;



### readDir

    array SB\Tools\Common::readDir($dirPath, string $type)

Читает директорию, возвращает список полного пути до файлов или каталогов



* Visibility: **public**
* This method is **static**.


#### Arguments
* $dirPath **mixed** - &lt;ul&gt;
&lt;li&gt;полный путь до читаемой директории&lt;/li&gt;
&lt;/ul&gt;
* $type **string** - &lt;ul&gt;
&lt;li&gt;(file|dir) тип получаемых путей&lt;/li&gt;
&lt;/ul&gt;



### iconvArray

    mixed SB\Tools\Common::iconvArray($ar, $inCharset, $outCharset)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $ar **mixed**
* $inCharset **mixed**
* $outCharset **mixed**



### getTree

    array SB\Tools\Common::getTree(array $arResult, array $arKeys, callable $functionFilter)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $arResult **array**
* $arKeys **array** - &lt;p&gt;массив ключей. Доступные значения id, parent_id, children. Могут быть переназначены на нужные значения, по дефолту ID, PARENT_ID, CHILDREN&lt;/p&gt;
* $functionFilter **callable** - &lt;p&gt;может быть передана функция (можно передавать строку, либо саму функцию), в функцию будет передан один параметр, текущий элемент. При возвращении функцией try удаляет элемент и всех его детей из результирующего массива&lt;/p&gt;



### getForPath

    boolean|mixed SB\Tools\Common::getForPath($path, $data)

рекурсивный поиск в массиве по ключам



* Visibility: **public**
* This method is **static**.


#### Arguments
* $path **mixed**
* $data **mixed** - &lt;p&gt;&lt;code&gt;
getForPath(array(
PROPERTIES =&gt; array(
NAME =&gt; VALUE
),
array(
PROPERTIES =&gt; array(
TEST =&gt; array(
NAME =&gt; &#039;ttt&#039;,
VALUE =&gt; 123
),
NAME =&gt; array(
NAME =&gt; &#039;name&#039;,
VALUE =&gt; 456
),
TEST2 =&gt; array(
NAME =&gt; &#039;ttt2&#039;,
VALUE =&gt; 789
)
)
)
));
result:
&lt;code&gt;123&lt;/code&gt;
&lt;/code&gt;&lt;/p&gt;



### setForPath

    boolean SB\Tools\Common::setForPath($path, $data, $value)

установить значение в массиве по пути
пример смотреть в getForPath



* Visibility: **public**
* This method is **static**.


#### Arguments
* $path **mixed**
* $data **mixed**
* $value **mixed**



### removeBuffer

    mixed SB\Tools\Common::removeBuffer()





* Visibility: **public**
* This method is **static**.



