SB\Pagination\PageCounter
===============






* Class name: PageCounter
* Namespace: SB\Pagination





Properties
----------


### $name

    protected mixed $name





* Visibility: **protected**


### $pageElementCount

    protected mixed $pageElementCount = array(9, 36, 90)





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Pagination\PageCounter::__construct($name, array $arElementCount)





* Visibility: **public**


#### Arguments
* $name **mixed**
* $arElementCount **array**



### getCountArray

    array SB\Pagination\PageCounter::getCountArray()





* Visibility: **public**




### getName

    string SB\Pagination\PageCounter::getName()





* Visibility: **public**




### getCount

    integer SB\Pagination\PageCounter::getCount(integer $defaultCount)

получение текущего количества, хранится в сессии



* Visibility: **public**


#### Arguments
* $defaultCount **integer**


