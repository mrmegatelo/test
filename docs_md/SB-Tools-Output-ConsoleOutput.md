SB\Tools\Output\ConsoleOutput
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: ConsoleOutput
* Namespace: SB\Tools\Output
* Parent class: [SB\Tools\Output](SB-Tools-Output.md)







Methods
-------


### __construct

    mixed SB\Tools\Output\ConsoleOutput::__construct($removeBuffer)





* Visibility: **public**


#### Arguments
* $removeBuffer **mixed**



### write

    mixed SB\Tools\Output::write($text)





* Visibility: **public**
* This method is **abstract**.
* This method is defined by [SB\Tools\Output](SB-Tools-Output.md)


#### Arguments
* $text **mixed**



### writeln

    mixed SB\Tools\Output::writeln($text)





* Visibility: **public**
* This method is **abstract**.
* This method is defined by [SB\Tools\Output](SB-Tools-Output.md)


#### Arguments
* $text **mixed**


