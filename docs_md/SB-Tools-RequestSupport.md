SB\Tools\RequestSupport
===============

Хранит данные для вывода
Class RequestSupport




* Class name: RequestSupport
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $_data

    protected mixed $_data = array()





* Visibility: **protected**


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### write

    mixed SB\Tools\RequestSupport::write($name, $value)





* Visibility: **public**


#### Arguments
* $name **mixed**
* $value **mixed**



### getData

    mixed SB\Tools\RequestSupport::getData()





* Visibility: **public**




### getInstance

    static SB\Tools\RequestSupport::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Tools\RequestSupport::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Tools\RequestSupport::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Tools\RequestSupport::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Tools\RequestSupport::__clone()





* Visibility: **protected**



