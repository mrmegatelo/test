SB\Bitrix\Tools\ImportCatalog1C
===============

Класс реализует протокол обмена номенклатурой 1с и сайта
Осуществляет загрузку наменклатуры.

Class ImportCatalog1C


* Class name: ImportCatalog1C
* Namespace: SB\Bitrix\Tools
* Parent class: [SB\Bitrix\Tools](SB-Bitrix-Tools.md)





Properties
----------


### $protocol

    protected mixed $protocol = "http"





* Visibility: **protected**


### $host

    protected mixed $host





* Visibility: **protected**


### $user

    protected mixed $user





* Visibility: **protected**


### $password

    protected mixed $password





* Visibility: **protected**


### $handlerUrl

    protected mixed $handlerUrl = "/bitrix/admin/1c_exchange.php"





* Visibility: **protected**


### $output

    protected \SB\Bitrix\Tools\Output $output





* Visibility: **protected**


### $authCookie

    protected mixed $authCookie = ''





* Visibility: **protected**


### $arInitResponse

    protected mixed $arInitResponse = array()





* Visibility: **protected**


### $timeout

    public mixed $timeout = 180





* Visibility: **public**


### $responseCharset

    public mixed $responseCharset = 'WINDOWS-1251'





* Visibility: **public**


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### __construct

    mixed SB\Bitrix\Tools::__construct($argument)





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $argument **mixed**



### setProtocol

    mixed SB\Bitrix\Tools\ImportCatalog1C::setProtocol(string $protocol)





* Visibility: **public**


#### Arguments
* $protocol **string**



### setHandlerUrl

    mixed SB\Bitrix\Tools\ImportCatalog1C::setHandlerUrl(string $handlerUrl)





* Visibility: **public**


#### Arguments
* $handlerUrl **string**



### setOutput

    mixed SB\Bitrix\Tools\ImportCatalog1C::setOutput(\SB\Bitrix\Tools\Output $output)





* Visibility: **public**


#### Arguments
* $output **SB\Bitrix\Tools\Output**



### setResponseCharset

    mixed SB\Bitrix\Tools\ImportCatalog1C::setResponseCharset(string $responseCharset)





* Visibility: **public**


#### Arguments
* $responseCharset **string**



### importFile

    mixed SB\Bitrix\Tools\ImportCatalog1C::importFile($filePath)

Выполняет передачу и загрузку файла в БД



* Visibility: **public**


#### Arguments
* $filePath **mixed**



### importDirectory

    mixed SB\Bitrix\Tools\ImportCatalog1C::importDirectory($dirPath)

Загружает все файлы, котрые есть в каталоге и подкаталоге как в папке webdata



* Visibility: **public**


#### Arguments
* $dirPath **mixed**



### checkAuthRequest

    mixed SB\Bitrix\Tools\ImportCatalog1C::checkAuthRequest($user, $password)

Отправляет запрос на авторизацию mode=checkauth type=catalog



* Visibility: **public**


#### Arguments
* $user **mixed**
* $password **mixed**



### initRequest

    string SB\Bitrix\Tools\ImportCatalog1C::initRequest()

Запрос параметров обмена  сайтас

TODO: Сейчас не работае архивирование. надо доделать

* Visibility: **public**




### uploadFile

    mixed SB\Bitrix\Tools\ImportCatalog1C::uploadFile(string $srcFilePath)

Пыполняет запросы по отправки файла на сайт



* Visibility: **public**


#### Arguments
* $srcFilePath **string**



### fileRequest

    string SB\Bitrix\Tools\ImportCatalog1C::fileRequest(string $filename, mixed $data)

Запрос отправки файла на сайт



* Visibility: **public**


#### Arguments
* $filename **string**
* $data **mixed**



### importRequest

    string SB\Bitrix\Tools\ImportCatalog1C::importRequest(string $filename)

Запрос параметров обмена  сайтас



* Visibility: **public**


#### Arguments
* $filename **string**



### sort

    integer SB\Bitrix\Tools\ImportCatalog1C::sort($a, $b)

сортировка файлов



* Visibility: **protected**


#### Arguments
* $a **mixed**
* $b **mixed**



### getUrl

    string SB\Bitrix\Tools\ImportCatalog1C::getUrl()

формирует url



* Visibility: **protected**




### request

    string SB\Bitrix\Tools\ImportCatalog1C::request($url, array $arOptions)

Посылвает запрос на сайт



* Visibility: **protected**


#### Arguments
* $url **mixed**
* $arOptions **array** - &lt;ul&gt;
&lt;li&gt;парамктры курл&lt;/li&gt;
&lt;/ul&gt;



### log

    mixed SB\Bitrix\Tools\ImportCatalog1C::log($text)





* Visibility: **protected**


#### Arguments
* $text **mixed**



### loadModules

    mixed SB\Bitrix\Tools::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Tools::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### getInstance

    static SB\Bitrix\Tools::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $argument **array**



### reset

    mixed SB\Bitrix\Tools::reset()





* Visibility: **public**
* This method is **static**.
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### __wakeup

    mixed SB\Bitrix\Tools::__wakeup()





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### __clone

    mixed SB\Bitrix\Tools::__clone()





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)



