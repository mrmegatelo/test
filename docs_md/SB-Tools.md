SB\Tools
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Tools
* Namespace: SB
* This is an **abstract** class
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md), [SB\Interfaces\Tool](SB-Interfaces-Tool.md)







