SB\Bitrix\Api\Cart
===============

API Cart
Работа с корзиной




* Class name: Cart
* Namespace: SB\Bitrix\Api
* Parent class: [SB\Bitrix\Api](SB-Bitrix-Api.md)





Properties
----------


### $basket

    public mixed $basket = null





* Visibility: **public**


### $customer

    public mixed $customer = null





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Bitrix\Api::__construct()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)




### refreshCart

    mixed SB\Bitrix\Api\Cart::refreshCart()





* Visibility: **public**




### getCountItems

    mixed SB\Bitrix\Api\Cart::getCountItems()





* Visibility: **public**




### loadModules

    mixed SB\Bitrix\Api::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Api::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)



