SB\Provider\Skeleton\ResponseTransformer
===============

Class ResponseTransformer




* Class name: ResponseTransformer
* Namespace: SB\Provider\Skeleton
* This is an **abstract** class





Properties
----------


### $configuration

    public \SB\Provider\Skeleton\Configuration $configuration = null





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Provider\Skeleton\ResponseTransformer::__construct(\SB\Provider\Skeleton\Configuration $configuration)

ResponseTransformer constructor.



* Visibility: **public**


#### Arguments
* $configuration **[SB\Provider\Skeleton\Configuration](SB-Provider-Skeleton-Configuration.md)**



### toString

    string SB\Provider\Skeleton\ResponseTransformer::toString(mixed $response)





* Visibility: **public**
* This method is **abstract**.


#### Arguments
* $response **mixed**


