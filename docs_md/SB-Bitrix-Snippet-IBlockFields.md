SB\Bitrix\Snippet\IBlockFields
===============

Class IBlockFields
Сниппеты Инфоблока




* Class name: IBlockFields
* Namespace: SB\Bitrix\Snippet
* This is an **abstract** class
* Parent class: [SB\Snippet](SB-Snippet.md)



Constants
----------


### CODE

    const CODE = array('IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => array('UNIQUE' => 'Y', 'TRANSLITERATION' => 'Y', 'TRANS_LEN' => '100', 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '-', 'TRANS_OTHER' => '-', 'TRANS_EAT' => 'Y'))








