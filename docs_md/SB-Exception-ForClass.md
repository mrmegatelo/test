SB\Exception\ForClass
===============

Class ForClass




* Class name: ForClass
* Namespace: SB\Exception
* Parent class: [SB\Exception](SB-Exception.md)





Properties
----------


### $className

    protected string $className = ''





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Exception::__construct(string $message, integer $code, \SB\Interfaces\Throwable|null $previous)

Exception constructor.



* Visibility: **public**
* This method is defined by [SB\Exception](SB-Exception.md)


#### Arguments
* $message **string**
* $code **integer**
* $previous **[SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)|null**


