SB\Exception\ForClass\ForProperty\NotExist
===============

Class NotExist




* Class name: NotExist
* Namespace: SB\Exception\ForClass\ForProperty
* Parent class: [SB\Exception\ForClass\ForProperty](SB-Exception-ForClass-ForProperty.md)





Properties
----------


### $propertyName

    protected string $propertyName = ''





* Visibility: **protected**


### $className

    protected string $className = ''





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Exception::__construct(string $message, integer $code, \SB\Interfaces\Throwable|null $previous)

Exception constructor.



* Visibility: **public**
* This method is defined by [SB\Exception](SB-Exception.md)


#### Arguments
* $message **string**
* $code **integer**
* $previous **[SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)|null**


