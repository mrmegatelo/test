SB\Service\Personal
===============






* Class name: Personal
* Namespace: SB\Service





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### login

    mixed SB\Service\Personal::login(\SB\Model\User $user, \SB\Service\bool $fromStorage)





* Visibility: **public**


#### Arguments
* $user **SB\Model\User**
* $fromStorage **SB\Service\bool**



### changeData

    mixed SB\Service\Personal::changeData(\SB\Model\User $user)





* Visibility: **public**


#### Arguments
* $user **SB\Model\User**



### loginForgot

    mixed SB\Service\Personal::loginForgot(\SB\Model\User $user, \SB\Service\string $code)





* Visibility: **public**


#### Arguments
* $user **SB\Model\User**
* $code **SB\Service\string**



### signUp

    mixed SB\Service\Personal::signUp(\SB\Model\User $user)





* Visibility: **public**


#### Arguments
* $user **SB\Model\User**



### changePassword

    mixed SB\Service\Personal::changePassword(\SB\Model\User $user)





* Visibility: **public**


#### Arguments
* $user **SB\Model\User**



### checkLogin

    mixed SB\Service\Personal::checkLogin($login)





* Visibility: **public**


#### Arguments
* $login **mixed**



### checkPhone

    mixed SB\Service\Personal::checkPhone($phone)





* Visibility: **public**


#### Arguments
* $phone **mixed**



### getInstance

    static SB\Service\Personal::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Service\Personal::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Service\Personal::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Service\Personal::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Service\Personal::__clone()





* Visibility: **protected**



