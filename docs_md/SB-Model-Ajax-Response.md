SB\Model\Ajax\Response
===============






* Class name: Response
* Namespace: SB\Model\Ajax





Properties
----------


### $status

    public mixed $status = true





* Visibility: **public**


### $data

    public mixed $data = array()





* Visibility: **public**


### $errors

    public mixed $errors = array()





* Visibility: **public**



