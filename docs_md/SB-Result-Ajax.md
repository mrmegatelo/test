SB\Result\Ajax
===============






* Class name: Ajax
* Namespace: SB\Result





Properties
----------


### $keys

    protected mixed $keys = array()





* Visibility: **protected**


### $status

    public mixed $status = true





* Visibility: **public**


### $data

    public mixed $data = array()





* Visibility: **public**


### $errors

    public mixed $errors = array()





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Result\Ajax::__construct($status, $data, $error)





* Visibility: **public**


#### Arguments
* $status **mixed**
* $data **mixed**
* $error **mixed**



### setStatusKey

    mixed SB\Result\Ajax::setStatusKey(string $key)





* Visibility: **public**


#### Arguments
* $key **string**



### getKey

    string SB\Result\Ajax::getKey(string $key)





* Visibility: **public**


#### Arguments
* $key **string**



### setDataKey

    mixed SB\Result\Ajax::setDataKey(string $key)





* Visibility: **public**


#### Arguments
* $key **string**



### setErrorKey

    mixed SB\Result\Ajax::setErrorKey(string $key)





* Visibility: **public**


#### Arguments
* $key **string**



### setStatus

    \SB\Result\Ajax SB\Result\Ajax::setStatus(boolean $status)





* Visibility: **public**


#### Arguments
* $status **boolean**



### addError

    \SB\Result\Ajax SB\Result\Ajax::addError(string $string, integer $code)





* Visibility: **public**


#### Arguments
* $string **string**
* $code **integer**



### addResult

    \SB\Result\Ajax SB\Result\Ajax::addResult(string $key, mixed $value)





* Visibility: **public**


#### Arguments
* $key **string**
* $value **mixed**



### getStatus

    mixed SB\Result\Ajax::getStatus()





* Visibility: **public**




### getData

    array SB\Result\Ajax::getData()





* Visibility: **public**




### getErrors

    array SB\Result\Ajax::getErrors()





* Visibility: **public**




### getAnswer

    array SB\Result\Ajax::getAnswer()





* Visibility: **public**



