SB\Service\City
===============

Сервис API: Работа с городами




* Class name: City
* Namespace: SB\Service





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getCityList

    array<mixed,\SB\IBlockElement\Service\City> SB\Service\City::getCityList()

Получить список городов
Не учитывает город
Не кэшируется



* Visibility: **public**




### getInstance

    static SB\Service\City::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Service\City::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Service\City::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Service\City::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Service\City::__clone()





* Visibility: **protected**



