SB\Bitrix\Entity
===============

Class Entity
Базовый класс сущностей




* Class name: Entity
* Namespace: SB\Bitrix
* This is an **abstract** class
* Parent class: [SB\Entity](SB-Entity.md)







Methods
-------


### __construct

    mixed SB\Bitrix\Entity::__construct()





* Visibility: **public**




### loadModules

    mixed SB\Bitrix\Entity::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Entity::getSiteId()





* Visibility: **public**



