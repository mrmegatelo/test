SB\Tools\ExcelReader
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: ExcelReader
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $delimiter

    public mixed $delimiter = ';'





* Visibility: **public**


### $startColumn

    private mixed $startColumn





* Visibility: **private**


### $endColumn

    private mixed $endColumn





* Visibility: **private**


### $maxCountEmptyRows

    public mixed $maxCountEmptyRows = 5





* Visibility: **public**


### $partSize

    private mixed $partSize = 300





* Visibility: **private**


### $readerType

    public mixed $readerType = 'Excel2007'





* Visibility: **public**


### $rowIterator

    private mixed $rowIterator





* Visibility: **private**


### $arColumns

    private mixed $arColumns = array()





* Visibility: **private**


### $book

    private mixed $book = false





* Visibility: **private**


### $filePath

    private mixed $filePath = false





* Visibility: **private**


Methods
-------


### __construct

    mixed SB\Tools\ExcelReader::__construct($filePath, boolean $book)





* Visibility: **public**


#### Arguments
* $filePath **mixed**
* $book **boolean** - &lt;ul&gt;
&lt;li&gt;Название книги в документе excel&lt;/li&gt;
&lt;/ul&gt;



### setRangeColumns

    mixed SB\Tools\ExcelReader::setRangeColumns($startColumn, $endColumn)

Устанавливает диапазон колонок, которые читаем



* Visibility: **public**


#### Arguments
* $startColumn **mixed**
* $endColumn **mixed**



### setStartRow

    mixed SB\Tools\ExcelReader::setStartRow($startRow)

Устанавливает номер строки с кторой читать данные



* Visibility: **public**


#### Arguments
* $startRow **mixed**



### getRowIterator

    mixed SB\Tools\ExcelReader::getRowIterator()





* Visibility: **public**




### setPartSize

    mixed SB\Tools\ExcelReader::setPartSize($partSize)

Устанавливает количество строки читаемое за раз



* Visibility: **public**


#### Arguments
* $partSize **mixed**



### readPartOfFile

    boolean SB\Tools\ExcelReader::readPartOfFile()

Читает часть excel файла



* Visibility: **public**




### getRows

    mixed SB\Tools\ExcelReader::getRows()





* Visibility: **public**




### saveToCsv

    mixed SB\Tools\ExcelReader::saveToCsv($filePath)

Сохраняет Данные в csv файл



* Visibility: **public**


#### Arguments
* $filePath **mixed**


