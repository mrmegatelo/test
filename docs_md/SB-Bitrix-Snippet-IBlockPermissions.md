SB\Bitrix\Snippet\IBlockPermissions
===============

Class IBlockPermissions
Спиппеты доступа к инфоблоку




* Class name: IBlockPermissions
* Namespace: SB\Bitrix\Snippet
* This is an **abstract** class
* Parent class: [SB\Snippet](SB-Snippet.md)



Constants
----------


### allRead

    const allRead = array('2' => 'R')








