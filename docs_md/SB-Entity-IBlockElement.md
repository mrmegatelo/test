SB\Entity\IBlockElement
===============

Class Entity
Базовый класс сущностей




* Class name: IBlockElement
* Namespace: SB\Entity
* Parent class: [SB\Entity](SB-Entity.md)





Properties
----------


### $code

    public mixed $code = null





* Visibility: **public**


### $id

    public mixed $id = null





* Visibility: **public**


### $name

    public mixed $name = null





* Visibility: **public**


### $sort

    public mixed $sort = 500





* Visibility: **public**


### $iBlock

    public mixed $iBlock = null





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Entity\IBlockElement::__construct(\SB\Entity\IBlock $iBlock)

IBlockElement конструктор класса



* Visibility: **public**


#### Arguments
* $iBlock **SB\Entity\IBlock**


