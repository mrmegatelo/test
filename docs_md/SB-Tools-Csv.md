SB\Tools\Csv
===============

Class Csv




* Class name: Csv
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### read

    mixed SB\Tools\Csv::read($fileName, $separator)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $fileName **mixed**
* $separator **mixed**



### write

    mixed SB\Tools\Csv::write($rewriteName, array $csv, $separator)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $rewriteName **mixed**
* $csv **array**
* $separator **mixed**


