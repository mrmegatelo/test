SB\Exception
===============

Class Exception
Базовый класс исключений
Описывает общие ошибки работы скриптов




* Class name: Exception
* Namespace: SB
* Parent class: Exception
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md), [SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)






Methods
-------


### __construct

    mixed SB\Exception::__construct(string $message, integer $code, \SB\Interfaces\Throwable|null $previous)

Exception constructor.



* Visibility: **public**


#### Arguments
* $message **string**
* $code **integer**
* $previous **[SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)|null**


