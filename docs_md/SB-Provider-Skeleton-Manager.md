SB\Provider\Skeleton\Manager
===============

Class Manager




* Class name: Manager
* Namespace: SB\Provider\Skeleton
* This is an **abstract** class





Properties
----------


### $configuration

    public \SB\Provider\Skeleton\Configuration $configuration = null





* Visibility: **public**


### $queryBuilder

    public \SB\Provider\Skeleton\QueryBuilder $queryBuilder





* Visibility: **public**


### $responseTransformer

    public \SB\Provider\Skeleton\ResponseTransformer $responseTransformer





* Visibility: **public**


### $requestSender

    public \SB\Provider\Skeleton\RequestSender $requestSender





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Provider\Skeleton\Manager::__construct(\SB\Provider\Skeleton\Configuration $configuration, \SB\Provider\Skeleton\QueryBuilder $queryBuilder, \SB\Provider\Skeleton\RequestSender $requestSender, \SB\Provider\Skeleton\ResponseTransformer $responseTransformer)

Manager constructor.



* Visibility: **protected**


#### Arguments
* $configuration **[SB\Provider\Skeleton\Configuration](SB-Provider-Skeleton-Configuration.md)**
* $queryBuilder **[SB\Provider\Skeleton\QueryBuilder](SB-Provider-Skeleton-QueryBuilder.md)**
* $requestSender **[SB\Provider\Skeleton\RequestSender](SB-Provider-Skeleton-RequestSender.md)**
* $responseTransformer **[SB\Provider\Skeleton\ResponseTransformer](SB-Provider-Skeleton-ResponseTransformer.md)**


