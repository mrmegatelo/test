SB\Bitrix\Api\Catalog
===============

Class Api




* Class name: Catalog
* Namespace: SB\Bitrix\Api
* Parent class: [SB\Bitrix\Api](SB-Bitrix-Api.md)





Properties
----------


### $obOffers

    private mixed $obOffers = null





* Visibility: **private**


### $obGoods

    private mixed $obGoods = null





* Visibility: **private**


### $skuInfo

    private mixed $skuInfo = null





* Visibility: **private**


### $cache

    private mixed $cache = null





* Visibility: **private**


### $cacheTtl

    private mixed $cacheTtl = 60 * 60 * 60





* Visibility: **private**


### $defaultSortType

    private mixed $defaultSortType = \SB\Snippet\OrderType::byIdDesc





* Visibility: **private**


### $useCache

    private mixed $useCache = true





* Visibility: **private**


Methods
-------


### __construct

    mixed SB\Bitrix\Api::__construct()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)




### getSectionList

    array SB\Bitrix\Api\Catalog::getSectionList()

Получение разделов каталога



* Visibility: **public**




### calcSectionList

    mixed SB\Bitrix\Api\Catalog::calcSectionList()





* Visibility: **public**




### search

    mixed SB\Bitrix\Api\Catalog::search($arParams)





* Visibility: **public**


#### Arguments
* $arParams **mixed**



### getProductList

    mixed SB\Bitrix\Api\Catalog::getProductList($arOffersList, $arGoodsList, $arLinkGoodsToOffers, $sortType)





* Visibility: **public**


#### Arguments
* $arOffersList **mixed**
* $arGoodsList **mixed**
* $arLinkGoodsToOffers **mixed**
* $sortType **mixed**



### sortByPriceAsc

    mixed SB\Bitrix\Api\Catalog::sortByPriceAsc($arProductList)





* Visibility: **public**


#### Arguments
* $arProductList **mixed**



### sortByPriceDesc

    mixed SB\Bitrix\Api\Catalog::sortByPriceDesc($arProductList)





* Visibility: **public**


#### Arguments
* $arProductList **mixed**



### buyProduct

    mixed SB\Bitrix\Api\Catalog::buyProduct($arParams)





* Visibility: **public**


#### Arguments
* $arParams **mixed**



### getParametersForSection

    mixed SB\Bitrix\Api\Catalog::getParametersForSection($arParams)





* Visibility: **public**


#### Arguments
* $arParams **mixed**



### getProduct

    mixed SB\Bitrix\Api\Catalog::getProduct($arParams)





* Visibility: **public**


#### Arguments
* $arParams **mixed**



### loadModules

    mixed SB\Bitrix\Api::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Api::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Api](SB-Bitrix-Api.md)



