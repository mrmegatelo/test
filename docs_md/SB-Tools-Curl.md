SB\Tools\Curl
===============

Class Curl




* Class name: Curl
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### post

    mixed SB\Tools\Curl::post($url, $headers, $postData)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $url **mixed**
* $headers **mixed**
* $postData **mixed**


