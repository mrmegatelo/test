SB\Provider\Skeleton\Configuration
===============

Class Configuration




* Class name: Configuration
* Namespace: SB\Provider\Skeleton
* This is an **abstract** class







Methods
-------


### __construct

    mixed SB\Provider\Skeleton\Configuration::__construct(array $arSetting)

Configuration constructor.



* Visibility: **protected**


#### Arguments
* $arSetting **array**


