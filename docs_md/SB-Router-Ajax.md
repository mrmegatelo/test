SB\Router\Ajax
===============

Роутер для Ajax запросов
Class Ajax




* Class name: Ajax
* Namespace: SB\Router





Properties
----------


### $response

    protected \SB\Response\Ajax $response





* Visibility: **protected**


### $controllerName

    protected string $controllerName





* Visibility: **protected**


### $method

    protected string $method





* Visibility: **protected**


### $classData

    protected array $classData = array('SB', 'Site', 'AjaxController')





* Visibility: **protected**


### $controllerReflection

    protected \ReflectionClass $controllerReflection





* Visibility: **protected**


### $controller

    protected \SB\Handler\Ajax $controller





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Router\Ajax::__construct(string $controllerName, string $method)

Ajax constructor.



* Visibility: **public**


#### Arguments
* $controllerName **string** - &lt;ul&gt;
&lt;li&gt;Имя класса контроллера&lt;/li&gt;
&lt;/ul&gt;
* $method **string** - &lt;ul&gt;
&lt;li&gt;Имя метода&lt;/li&gt;
&lt;/ul&gt;



### execute

    \SB\Model\Ajax\Response SB\Router\Ajax::execute()





* Visibility: **public**




### getController

    \SB\Handler\Ajax SB\Router\Ajax::getController()





* Visibility: **public**




### removeIndex

    mixed SB\Router\Ajax::removeIndex($path)





* Visibility: **protected**


#### Arguments
* $path **mixed**


