SB\Bitrix\Tools\IBlock
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: IBlock
* Namespace: SB\Bitrix\Tools
* Parent class: [SB\Bitrix\Tools](SB-Bitrix-Tools.md)





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getDisplayProperty

    null SB\Bitrix\Tools\IBlock::getDisplayProperty($arItem, $name, string $field)





* Visibility: **public**


#### Arguments
* $arItem **mixed**
* $name **mixed**
* $field **string**



### getProperty

    mixed SB\Bitrix\Tools\IBlock::getProperty($arItem, $name, $field)





* Visibility: **public**


#### Arguments
* $arItem **mixed**
* $name **mixed**
* $field **mixed**



### GetPropertyValuesArray

    mixed SB\Bitrix\Tools\IBlock::GetPropertyValuesArray($result, $iblockID, $filter, $propertyFilter)





* Visibility: **public**


#### Arguments
* $result **mixed**
* $iblockID **mixed**
* $filter **mixed**
* $propertyFilter **mixed**



### getElement

    mixed SB\Bitrix\Tools\IBlock::getElement($arFilter, $arSelectFields, $arGroupBy)





* Visibility: **public**


#### Arguments
* $arFilter **mixed**
* $arSelectFields **mixed**
* $arGroupBy **mixed**



### getElements

    mixed SB\Bitrix\Tools\IBlock::getElements($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields)





* Visibility: **public**


#### Arguments
* $arOrder **mixed**
* $arFilter **mixed**
* $arGroupBy **mixed**
* $arNavStartParams **mixed**
* $arSelectFields **mixed**



### getElementById

    mixed SB\Bitrix\Tools\IBlock::getElementById($id)





* Visibility: **public**


#### Arguments
* $id **mixed**



### getSection

    mixed SB\Bitrix\Tools\IBlock::getSection($arFilter, $arSelect)





* Visibility: **public**


#### Arguments
* $arFilter **mixed**
* $arSelect **mixed**



### getSections

    mixed SB\Bitrix\Tools\IBlock::getSections($arOrder, $arFilter, $bIncCnt, $arSelect, $arNavStartParams)





* Visibility: **public**


#### Arguments
* $arOrder **mixed**
* $arFilter **mixed**
* $bIncCnt **mixed**
* $arSelect **mixed**
* $arNavStartParams **mixed**



### getSectionById

    mixed SB\Bitrix\Tools\IBlock::getSectionById($id)





* Visibility: **public**


#### Arguments
* $id **mixed**



### saveElement

    integer|null SB\Bitrix\Tools\IBlock::saveElement($arFields, boolean $prepareFields, boolean $checkXmlId)





* Visibility: **public**


#### Arguments
* $arFields **mixed** - &lt;ul&gt;
&lt;li&gt;Поля елемента. Если указан [&#039;ID&#039;] - то выполняется Update&lt;/li&gt;
&lt;/ul&gt;
* $prepareFields **boolean** - &lt;ul&gt;
&lt;li&gt;Флаг обработки полей, заполняет некоторые поля (IBLOCK_ID,ACTIVE,CODE,MODIFIED_BY)&lt;/li&gt;
&lt;/ul&gt;
* $checkXmlId **boolean** - &lt;ul&gt;
&lt;li&gt;Флаг Получения эле елемента по XML_ID. В случаи нахождения элемента выполняется Update&lt;/li&gt;
&lt;/ul&gt;



### saveElementOld

    mixed SB\Bitrix\Tools\IBlock::saveElementOld($iBlockId, $arFields, $prepareFields, $checkXmlId)





* Visibility: **public**


#### Arguments
* $iBlockId **mixed**
* $arFields **mixed**
* $prepareFields **mixed**
* $checkXmlId **mixed**



### fillImageArray

    mixed SB\Bitrix\Tools\IBlock::fillImageArray($arItem, array $arFields)

Заполняет массивами картинок ключи с ид картинок



* Visibility: **public**


#### Arguments
* $arItem **mixed**
* $arFields **array**



### clearIBlock

    mixed SB\Bitrix\Tools\IBlock::clearIBlock($iBlockId)





* Visibility: **public**


#### Arguments
* $iBlockId **mixed**



### iBlockCopy

    boolean SB\Bitrix\Tools\IBlock::iBlockCopy($oldIBlock, array $arParams, boolean $properties)

Копирование инфоблока



* Visibility: **public**


#### Arguments
* $oldIBlock **mixed** - &lt;p&gt;(ид инфоблока)&lt;/p&gt;
* $arParams **array** - &lt;p&gt;(параметры, которые нужно заменить)&lt;/p&gt;
* $properties **boolean** - &lt;p&gt;(копировать свойства)&lt;/p&gt;



### propertyCopy

    mixed SB\Bitrix\Tools\IBlock::propertyCopy($oldIBlock, $newIBlock)

Копирование свойст инфоблока



* Visibility: **public**


#### Arguments
* $oldIBlock **mixed**
* $newIBlock **mixed**



### getIBlockProperties

    array SB\Bitrix\Tools\IBlock::getIBlockProperties($IBlockID)





* Visibility: **public**


#### Arguments
* $IBlockID **mixed**



### __construct

    mixed SB\Bitrix\Tools::__construct($argument)





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $argument **mixed**



### loadModules

    mixed SB\Bitrix\Tools::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Tools::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### getInstance

    static SB\Bitrix\Tools::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $argument **array**



### reset

    mixed SB\Bitrix\Tools::reset()





* Visibility: **public**
* This method is **static**.
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### __wakeup

    mixed SB\Bitrix\Tools::__wakeup()





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### __clone

    mixed SB\Bitrix\Tools::__clone()





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)



