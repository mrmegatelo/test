SB\Bitrix\Entity\IBlock
===============

Сущность &quot;Инфоблок&quot;
Содержит общие свойства и методы




* Class name: IBlock
* Namespace: SB\Bitrix\Entity
* Parent class: [SB\Bitrix\Entity](SB-Bitrix-Entity.md)





Properties
----------


### $id

    public  $id





* Visibility: **public**


### $code

    public  $code





* Visibility: **public**


### $name

    public  $name





* Visibility: **public**


### $sort

    public  $sort





* Visibility: **public**


### $iBlockType

    public  $iBlockType





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Bitrix\Entity::__construct()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Entity](SB-Bitrix-Entity.md)




### __get

    mixed SB\Bitrix\Entity\IBlock::__get(string $name)





* Visibility: **public**


#### Arguments
* $name **string**



### __getInfo

    mixed SB\Bitrix\Entity\IBlock::__getInfo()





* Visibility: **protected**




### isExist

    integer|boolean SB\Bitrix\Entity\IBlock::isExist()

Проверяет существование



* Visibility: **public**




### getById

    mixed SB\Bitrix\Entity\IBlock::getById($id)

Возвращает инфоблок по ID



* Visibility: **public**
* This method is **static**.


#### Arguments
* $id **mixed**



### getByFilter

    mixed SB\Bitrix\Entity\IBlock::getByFilter($arFilter)

Возвращает массив инфоблоков



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arFilter **mixed**



### create

    boolean SB\Bitrix\Entity\IBlock::create(array $arFieldsParameters)

Создает инфоблок



* Visibility: **public**


#### Arguments
* $arFieldsParameters **array**



### createElements

    array SB\Bitrix\Entity\IBlock::createElements($arElements)

Создание списка элементов



* Visibility: **public**


#### Arguments
* $arElements **mixed**



### createElement

    boolean SB\Bitrix\Entity\IBlock::createElement($arData)

Создает элемент инфоблока



* Visibility: **public**


#### Arguments
* $arData **mixed**



### getElements

    array|\CIBlockResult SB\Bitrix\Entity\IBlock::getElements(array $additionalFilter, array $additionalSelect, array $arOrder, integer $limit, boolean $fetch)

Получает список элементов



* Visibility: **public**


#### Arguments
* $additionalFilter **array**
* $additionalSelect **array**
* $arOrder **array**
* $limit **integer**
* $fetch **boolean**



### getElementIdByCode

    integer SB\Bitrix\Entity\IBlock::getElementIdByCode($iblockId, $elementCode, array $arFilterAdditions)

Возвращает элемент по коду



* Visibility: **public**


#### Arguments
* $iblockId **mixed**
* $elementCode **mixed**
* $arFilterAdditions **array**



### getPropertyEnum

    array SB\Bitrix\Entity\IBlock::getPropertyEnum($propertyCode)

Получает значения свойств типа список



* Visibility: **public**


#### Arguments
* $propertyCode **mixed**



### loadModules

    mixed SB\Bitrix\Entity::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Entity](SB-Bitrix-Entity.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Entity::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Entity](SB-Bitrix-Entity.md)



