SB\Tools\Dumper
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Dumper
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $iLevel

    private mixed $iLevel





* Visibility: **private**
* This property is **static**.


### $aObjects

    private mixed $aObjects = array()





* Visibility: **private**
* This property is **static**.


Methods
-------


### dump

    mixed SB\Tools\Dumper::dump($aArgs)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $aArgs **mixed**



### dumpTrace

    mixed SB\Tools\Dumper::dumpTrace($aArgs)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $aArgs **mixed**



### get_item_dump

    mixed SB\Tools\Dumper::get_item_dump($mItem)





* Visibility: **private**
* This method is **static**.


#### Arguments
* $mItem **mixed**



### get_array_dump

    mixed SB\Tools\Dumper::get_array_dump($aItem)

{
  [1] => .

..
  [2] => ...
}

* Visibility: **private**
* This method is **static**.


#### Arguments
* $aItem **mixed**



### get_object_dump

    string SB\Tools\Dumper::get_object_dump($mItem)





* Visibility: **private**
* This method is **static**.


#### Arguments
* $mItem **mixed**



### add_shift_if_multiline

    mixed SB\Tools\Dumper::add_shift_if_multiline($sText, $sShift)





* Visibility: **private**
* This method is **static**.


#### Arguments
* $sText **mixed**
* $sShift **mixed**


