SB\Manager\City
===============

Содержит функции разделения контента по городам




* Class name: City
* Namespace: SB\Manager





Properties
----------


### $city

    public object $city = null





* Visibility: **public**


### $defaultCityCode

    protected string $defaultCityCode = 'krasnoyarsk'





* Visibility: **protected**


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### __construct

    mixed SB\Manager\City::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### setCity

    boolean SB\Manager\City::setCity($city)

Устанавливает город по коду



* Visibility: **public**


#### Arguments
* $city **mixed**



### getCity

    \SB\Manager\City SB\Manager\City::getCity()

Возаращает объект текущего города или null



* Visibility: **public**




### getCityId

    integer SB\Manager\City::getCityId()

Возвращает ID текущего города или 0



* Visibility: **public**




### getCityCode

    string SB\Manager\City::getCityCode()

Возвращает символьный код текущего города или пустую строку



* Visibility: **public**




### getCityName

    string SB\Manager\City::getCityName()

Возвращает имя текущего города или пустую строку



* Visibility: **public**




### getPharmacy

    array SB\Manager\City::getPharmacy()

Возвращает список кодов аптек текущего города или пустой массив



* Visibility: **public**




### detectCity

    boolean SB\Manager\City::detectCity()

Определение города
по поддомену
по cookie
по IPаг
по символьному коду города по умолчанию



* Visibility: **public**




### getInstance

    static SB\Manager\City::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Manager\City::reset()





* Visibility: **public**
* This method is **static**.




### __wakeup

    mixed SB\Manager\City::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Manager\City::__clone()





* Visibility: **protected**



