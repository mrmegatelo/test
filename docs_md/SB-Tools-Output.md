SB\Tools\Output
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Output
* Namespace: SB\Tools
* This is an **abstract** class
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### write

    mixed SB\Tools\Output::write($text)





* Visibility: **public**
* This method is **abstract**.


#### Arguments
* $text **mixed**



### writeln

    mixed SB\Tools\Output::writeln($text)





* Visibility: **public**
* This method is **abstract**.


#### Arguments
* $text **mixed**


