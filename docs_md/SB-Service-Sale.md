SB\Service\Sale
===============

Сервис API: Работа с покупками




* Class name: Sale
* Namespace: SB\Service





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getCustomer

    \SB\Model\Customer SB\Service\Sale::getCustomer(integer|null $userId)

Получить покупателя



* Visibility: **public**


#### Arguments
* $userId **integer|null** - &lt;ul&gt;
&lt;li&gt;ИД пользователя&lt;/li&gt;
&lt;/ul&gt;



### getStoreList

    array<mixed,\SB\Model\Store> SB\Service\Sale::getStoreList(integer $customerId)

Получить список аптек



* Visibility: **public**


#### Arguments
* $customerId **integer** - &lt;ul&gt;
&lt;li&gt;ИД покупателя&lt;/li&gt;
&lt;/ul&gt;



### getSaleParameters

    \SB\Model\SaleParameters SB\Service\Sale::getSaleParameters(integer $storeCode)

Получить настройки оформления заказа



* Visibility: **public**


#### Arguments
* $storeCode **integer** - &lt;ul&gt;
&lt;li&gt;код аптеки&lt;/li&gt;
&lt;/ul&gt;



### getInstance

    static SB\Service\Sale::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Service\Sale::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Service\Sale::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Service\Sale::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Service\Sale::__clone()





* Visibility: **protected**



