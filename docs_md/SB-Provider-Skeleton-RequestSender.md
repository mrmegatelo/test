SB\Provider\Skeleton\RequestSender
===============

Class RequestSender




* Class name: RequestSender
* Namespace: SB\Provider\Skeleton
* This is an **abstract** class





Properties
----------


### $configuration

    public \SB\Provider\Skeleton\Configuration $configuration = null





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Provider\Skeleton\RequestSender::__construct(\SB\Provider\Skeleton\Configuration $configuration)

RequestSender constructor.



* Visibility: **public**


#### Arguments
* $configuration **[SB\Provider\Skeleton\Configuration](SB-Provider-Skeleton-Configuration.md)**



### send

    mixed SB\Provider\Skeleton\RequestSender::send(mixed $query, array $params)





* Visibility: **public**
* This method is **abstract**.


#### Arguments
* $query **mixed**
* $params **array**


