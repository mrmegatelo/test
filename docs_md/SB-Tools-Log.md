SB\Tools\Log
===============

Class Log




* Class name: Log
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $filePath

    private mixed $filePath = ''





* Visibility: **private**


### $fileRecourse

    private mixed $fileRecourse = null





* Visibility: **private**


Methods
-------


### __construct

    mixed SB\Tools\Log::__construct($filePath, $mode)





* Visibility: **public**


#### Arguments
* $filePath **mixed**
* $mode **mixed**



### add

    mixed SB\Tools\Log::add($message)





* Visibility: **public**


#### Arguments
* $message **mixed**



### makeRecord

    mixed SB\Tools\Log::makeRecord($message)





* Visibility: **private**
* This method is **static**.


#### Arguments
* $message **mixed**



### printFile

    mixed SB\Tools\Log::printFile()





* Visibility: **public**




### getFileContent

    mixed SB\Tools\Log::getFileContent()





* Visibility: **public**




### __destruct

    mixed SB\Tools\Log::__destruct()





* Visibility: **public**




### addToLog

    mixed SB\Tools\Log::addToLog($filePath, $message)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $filePath **mixed**
* $message **mixed**


