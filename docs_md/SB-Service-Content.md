SB\Service\Content
===============

Сервис API: Работа с динамическим контентом




* Class name: Content
* Namespace: SB\Service





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getSlideList

    mixed SB\Service\Content::getSlideList()

Запрос: получить спискок слайдов
Возвращает массив слайдов или ошибку
Не кэшируется
Не учитывает город



* Visibility: **public**




### getStockList

    array SB\Service\Content::getStockList(integer|null $count, boolean $useCache)

Запрос: получить спискок акций
Возвращает массив акций или ошибку
Учитывает город
Кэшируется



* Visibility: **public**


#### Arguments
* $count **integer|null** - &lt;ul&gt;
&lt;li&gt;количество записей&lt;/li&gt;
&lt;/ul&gt;
* $useCache **boolean**



### getAddressCityList

    array<mixed,\SB\IBlockElement\Service\City> SB\Service\Content::getAddressCityList()

Запрос: получить спискок городов для карты
Возвращает массив городов или ошибку
Кэшируется



* Visibility: **public**




### getAddressStoreList

    mixed SB\Service\Content::getAddressStoreList(\SB\Service\string $storeCode)





* Visibility: **public**


#### Arguments
* $storeCode **SB\Service\string**



### getInstance

    static SB\Service\Content::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Service\Content::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Service\Content::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Service\Content::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Service\Content::__clone()





* Visibility: **protected**



