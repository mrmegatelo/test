SB\Handler\Ajax
===============

Class Handler
Базовый класс обработчиков




* Class name: Ajax
* Namespace: SB\Handler
* Parent class: [SB\Handler](SB-Handler.md)





Properties
----------


### $result

    protected \SB\Model\Response $result





* Visibility: **protected**


### $request

    protected \Bitrix\Main\HttpRequest $request





* Visibility: **protected**


### $params

    protected array $params





* Visibility: **protected**


### $method

    protected string $method





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Handler\Ajax::__construct(\Bitrix\Main\HttpRequest $request, \SB\Model\Response $result)





* Visibility: **public**


#### Arguments
* $request **Bitrix\Main\HttpRequest**
* $result **SB\Model\Response**



### getResult

    mixed SB\Handler\Ajax::getResult()





* Visibility: **public**



