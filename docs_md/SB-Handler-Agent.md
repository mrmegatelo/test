SB\Handler\Agent
===============

Class Handler
Базовый класс обработчиков




* Class name: Agent
* Namespace: SB\Handler
* Parent class: [SB\Handler](SB-Handler.md)







Methods
-------


### exec

    string SB\Handler\Agent::exec(array $arParams)

выполнение функции, предназначено для выполнения агентов в обёртке,
для упразднения действий и возможности отладки через лог

результат из колбэк функции нужно возвращать массивом, в следующую колбэк функцию они попадут как распакованный
массив

* Visibility: **public**
* This method is **static**.


#### Arguments
* $arParams **array** - &lt;p&gt;описание параметров такие же как и в create
только нет $arParams[&#039;agentParams&#039;]&lt;/p&gt;



### create

    boolean|integer SB\Handler\Agent::create(array $arParams)

Создаёт агента в обёртке, для упрощения действий



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arParams **array** - &lt;p&gt;&lt;code&gt;
callable &#039;function&#039; (REQUIRED) =&gt; функция исполняемая агентом
array &#039;agentParams =&gt; ( параметры агента
string &#039;module&#039; =&gt; ид модуля,
string|int &#039;sort&#039; =&gt; сортировка,
string &#039;active&#039; =&gt; активность (&quot;Y&quot;/&quot;N&quot;),
int &#039;interval&#039; =&gt; интервал,
string &#039;period&#039; =&gt; периодичный ли агент (&quot;Y&quot;/&quot;N&quot;),
bool &#039;unique&#039; =&gt; уникальный ли агент
),
array &#039;logParams =&gt; ( параметры логирования
string &#039;name&#039; =&gt; имя класса, который реализует SB\Interfaces\Log,
mixed[] &#039;args&#039; =&gt; параметры для конструктора класса
),
mixed[] &#039;funcParams&#039; =&gt; параметры передаваемые в колбэк функцию,
bool &#039;repeat&#039; =&gt; повторяемая ли функция, default: true,
bool &#039;updateFuncParams&#039; =&gt; обновлять ли параметры функции, default: true
&lt;/code&gt;&lt;/p&gt;
&lt;p&gt;&lt;code&gt;
пример:
array(
&quot;function&quot; =&gt; array(&quot;\SB\Helper\Store&quot;, &quot;getPartnerStoresId&quot;),
&quot;agentParams&quot; =&gt; array(&#039;active&#039; =&gt; &quot;Y&quot;, &#039;interval&#039; =&gt; 86400, &#039;unique&#039; =&gt; true),
&quot;logParams&quot; =&gt; array(&quot;name&quot; =&gt; &quot;\SB\Log&quot;, &#039;args&#039; =&gt; array( $_SERVER[&#039;DOCUMENT_ROOT&#039;] . &#039;/upload/tmp/logs/test.txt&#039;))
)
&lt;/code&gt;&lt;/p&gt;



### checkFields

    mixed SB\Handler\Agent::checkFields(array $arParams)

проверяет передаваемые значения



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arParams **array**


