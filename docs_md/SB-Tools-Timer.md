SB\Tools\Timer
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Timer
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $Timer

    private mixed $Timer = null





* Visibility: **private**
* This property is **static**.


### $startPoint

    private mixed $startPoint = null





* Visibility: **private**


### $stopPoint

    private mixed $stopPoint = null





* Visibility: **private**


### $interval

    private mixed $interval = null





* Visibility: **private**


Methods
-------


### Instance

    mixed SB\Tools\Timer::Instance($new)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $new **mixed**



### start

    mixed SB\Tools\Timer::start()





* Visibility: **public**




### stop

    mixed SB\Tools\Timer::stop()





* Visibility: **public**




### getInterval

    mixed SB\Tools\Timer::getInterval($type)





* Visibility: **public**


#### Arguments
* $type **mixed**



### out

    mixed SB\Tools\Timer::out($type)





* Visibility: **public**


#### Arguments
* $type **mixed**


