SB\Tools\ForComponents
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: ForComponents
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### renderIncludeArea

    mixed SB\Tools\ForComponents::renderIncludeArea($path)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $path **mixed**


