SB\Tools\ValidationLang
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: ValidationLang
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)



Constants
----------


### MODE_DEFAULT

    const MODE_DEFAULT = 1





### MODE_NEGATIVE

    const MODE_NEGATIVE = 2





### STANDARD

    const STANDARD = 0





### EXTRA

    const EXTRA = 1





Properties
----------


### $arRu

    private mixed $arRu = array('alnum' => array(self::MODE_DEFAULT => array(self::STANDARD => '{{name}} должно содержать только символы (a-z), цифры (0-9)', self::EXTRA => '{{name}} должно содержать только символы (a-z), цифры (0-9) и {{additionalChars}}'), self::MODE_NEGATIVE => array(self::STANDARD => '{{name}} не должно содержать символы (a-z), цифры (0-9))', self::EXTRA => '{{name}} не должно содержать символы (a-z), цифры (0-9) и {{additionalChars}}')), 'numeric' => array(self::MODE_DEFAULT => array(self::STANDARD => '{{name}} должно быть числом'), self::MODE_NEGATIVE => array(self::STANDARD => '{{name}} не должно быть числом')))





* Visibility: **private**
* This property is **static**.


Methods
-------


### getMessage

    null SB\Tools\ValidationLang::getMessage($validator, boolean $standard, boolean $negative)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $validator **mixed**
* $standard **boolean**
* $negative **boolean**



### getMessages

    array SB\Tools\ValidationLang::getMessages($arValidators)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $arValidators **mixed**


