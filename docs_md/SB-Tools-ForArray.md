SB\Tools\ForArray
===============

Class ForArray




* Class name: ForArray
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### getMaxElementByKey

    mixed SB\Tools\ForArray::getMaxElementByKey($arData, $keyName)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $arData **mixed**
* $keyName **mixed**



### getMinElementByKey

    mixed SB\Tools\ForArray::getMinElementByKey($arData, $keyName)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $arData **mixed**
* $keyName **mixed**


