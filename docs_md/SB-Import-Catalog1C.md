SB\Import\Catalog1C
===============






* Class name: Catalog1C
* Namespace: SB\Import





Properties
----------


### $useLog

    protected mixed $useLog = true





* Visibility: **protected**


### $logFilePath

    protected mixed $logFilePath = '/upload/logs/import/catalog/Import of '





* Visibility: **protected**


### $logFileExtension

    protected mixed $logFileExtension = '.txt'





* Visibility: **protected**


### $logDateFormat

    protected mixed $logDateFormat = 'Y-m-d H:i:s'





* Visibility: **protected**


### $host

    protected mixed $host = ''





* Visibility: **protected**


### $login

    protected mixed $login = ''





* Visibility: **protected**


### $password

    protected mixed $password = ''





* Visibility: **protected**


### $optionModuleName

    protected mixed $optionModuleName = 'main'





* Visibility: **protected**


### $optionName

    protected mixed $optionName = 'last_import_catalog'





* Visibility: **protected**


### $errorOptionName

    protected mixed $errorOptionName = 'error_import_catalog'





* Visibility: **protected**


Methods
-------


### disableCheck

    mixed SB\Import\Catalog1C::disableCheck()





* Visibility: **public**




### Import

    mixed SB\Import\Catalog1C::Import($fileName, $type, $dump)





* Visibility: **public**


#### Arguments
* $fileName **mixed**
* $type **mixed**
* $dump **mixed**


