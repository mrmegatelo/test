SB\Pagination\PageCounterFactory
===============






* Class name: PageCounterFactory
* Namespace: SB\Pagination





Properties
----------


### $name

    protected mixed $name = 'show'





* Visibility: **protected**


### $counters

    protected array<mixed,\SB\Pagination\PageCounter> $counters = array()





* Visibility: **protected**


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getName

    string SB\Pagination\PageCounterFactory::getName()





* Visibility: **public**




### addCounter

    mixed SB\Pagination\PageCounterFactory::addCounter(\CBitrixComponent $component, array $arElementCount)





* Visibility: **public**


#### Arguments
* $component **CBitrixComponent**
* $arElementCount **array**



### getCounter

    mixed SB\Pagination\PageCounterFactory::getCounter(\CBitrixComponent $component)





* Visibility: **public**


#### Arguments
* $component **CBitrixComponent**



### getInstance

    static SB\Pagination\PageCounterFactory::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Pagination\PageCounterFactory::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Pagination\PageCounterFactory::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Pagination\PageCounterFactory::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Pagination\PageCounterFactory::__clone()





* Visibility: **protected**



