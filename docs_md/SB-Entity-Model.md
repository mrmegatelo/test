SB\Entity\Model
===============






* Class name: Model
* Namespace: SB\Entity







Methods
-------


### __construct

    mixed SB\Entity\Model::__construct(array $fields, $needTransform)





* Visibility: **public**


#### Arguments
* $fields **array**
* $needTransform **mixed**



### transform

    array SB\Entity\Model::transform(array $fields)





* Visibility: **public**


#### Arguments
* $fields **array**


