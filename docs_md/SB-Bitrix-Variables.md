SB\Bitrix\Variables
===============

Class Variables




* Class name: Variables
* Namespace: SB\Bitrix
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md)


Constants
----------


### MODULE_LIST_DEFAULT

    const MODULE_LIST_DEFAULT = array('main', 'iblock', 'sale', 'catalog', 'currency', 'search')





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getInstance

    static SB\Bitrix\Variables::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Bitrix\Variables::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Bitrix\Variables::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Bitrix\Variables::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Bitrix\Variables::__clone()





* Visibility: **protected**



