SB\Tools\Html
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Html
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $closeSingleTags

    public mixed $closeSingleTags = true





* Visibility: **public**
* This property is **static**.


Methods
-------


### a

    string SB\Tools\Html::a($sHref, $sContent, array $aAttrs)

Выводит тег a



* Visibility: **public**
* This method is **static**.


#### Arguments
* $sHref **mixed**
* $sContent **mixed**
* $aAttrs **array**



### renderAttributes

    string SB\Tools\Html::renderAttributes(array $aAttrs)

выводит атрибуты тега



* Visibility: **public**
* This method is **static**.


#### Arguments
* $aAttrs **array** - &lt;ul&gt;
&lt;li&gt;массив атрибутув
$aAttrs[&lt;название атрибута&gt;] = &lt;значение атрибута&gt;. Если &lt;значение атрибута&gt;===true, то выводится атрибут без значения, например selected&lt;/li&gt;
&lt;/ul&gt;



### tag

    string SB\Tools\Html::tag($sTag, boolean $sContent, array $aAttrs, boolean $bCloseTag)

Выводит тег



* Visibility: **public**
* This method is **static**.


#### Arguments
* $sTag **mixed** - &lt;ul&gt;
&lt;li&gt;название тега&lt;/li&gt;
&lt;/ul&gt;
* $sContent **boolean** - &lt;ul&gt;
&lt;li&gt;содержимое&lt;/li&gt;
&lt;/ul&gt;
* $aAttrs **array** - &lt;ul&gt;
&lt;li&gt;атрибуты тега&lt;/li&gt;
&lt;/ul&gt;
* $bCloseTag **boolean** - &lt;ul&gt;
&lt;li&gt;выводить закрвабщийся тег&lt;/li&gt;
&lt;/ul&gt;



### select

    string SB\Tools\Html::select($arItems, $arAttrs, boolean $selected, $arOptionsAttrs)

Выводит <select>

Пример:
$arElements = \SB\IBlock::getElements(array(),array('IBLOCK_ID'=>32), false, false, array('ID','NAME'));
$arItems = \SB\Tools::getList($arElements,'ID','NAME');
echo \SB\Html::select($arItems, array('name'=>'select1'));

* Visibility: **public**
* This method is **static**.


#### Arguments
* $arItems **mixed** - &lt;ul&gt;
&lt;li&gt;список элементов
$arItems[&lt;value&gt;] = &lt;text&gt;
$arItems[&lt;value&gt;] = array(&#039;text&#039;=&gt;&lt;text&gt;, &#039;attrs&#039;=&gt;&lt;массив атрибутов&gt;).&lt;/li&gt;
&lt;/ul&gt;
* $arAttrs **mixed** - &lt;ul&gt;
&lt;li&gt;атрибуты &lt;select&gt;&lt;/li&gt;
&lt;/ul&gt;
* $selected **boolean**
* $arOptionsAttrs **mixed**


