SB\Bitrix\Tools\Common
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Common
* Namespace: SB\Bitrix\Tools
* Parent class: [SB\Bitrix\Tools](SB-Bitrix-Tools.md)





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### userDump

    mixed SB\Bitrix\Tools\Common::userDump()

Делает дамп только для определенного пользователя
первый аргумент функции user_id, остальные переменыые для вывода



* Visibility: **public**




### isHome

    boolean SB\Bitrix\Tools\Common::isHome()

Проверяет является ли текущая директория домашней



* Visibility: **public**




### fetchDbResult

    array SB\Bitrix\Tools\Common::fetchDbResult($rsResult)

Преобразует dbResult в массив используя fetch



* Visibility: **public**


#### Arguments
* $rsResult **mixed**



### getSiteId

    mixed SB\Bitrix\Tools::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### __construct

    mixed SB\Bitrix\Tools::__construct($argument)





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $argument **mixed**



### loadModules

    mixed SB\Bitrix\Tools::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $idList **array**



### getInstance

    static SB\Bitrix\Tools::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)


#### Arguments
* $argument **array**



### reset

    mixed SB\Bitrix\Tools::reset()





* Visibility: **public**
* This method is **static**.
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### __wakeup

    mixed SB\Bitrix\Tools::__wakeup()





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)




### __clone

    mixed SB\Bitrix\Tools::__clone()





* Visibility: **protected**
* This method is defined by [SB\Bitrix\Tools](SB-Bitrix-Tools.md)



