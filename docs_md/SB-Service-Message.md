SB\Service\Message
===============






* Class name: Message
* Namespace: SB\Service





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### __construct

    mixed SB\Service\Message::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### send

    mixed SB\Service\Message::send(\SB\Service\string $phone, \SB\Service\string $message, \SB\Service\string $senderName)





* Visibility: **public**


#### Arguments
* $phone **SB\Service\string**
* $message **SB\Service\string**
* $senderName **SB\Service\string**



### getInstance

    static SB\Service\Message::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Service\Message::reset()





* Visibility: **public**
* This method is **static**.




### __wakeup

    mixed SB\Service\Message::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Service\Message::__clone()





* Visibility: **protected**



