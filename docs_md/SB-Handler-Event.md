SB\Handler\Event
===============

Class Handler
Базовый класс обработчиков




* Class name: Event
* Namespace: SB\Handler
* Parent class: [SB\Handler](SB-Handler.md)





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### __construct

    mixed SB\Handler\Event::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### getInstance

    static SB\Handler\Event::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Handler\Event::reset()





* Visibility: **public**
* This method is **static**.




### __wakeup

    mixed SB\Handler\Event::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Handler\Event::__clone()





* Visibility: **protected**



