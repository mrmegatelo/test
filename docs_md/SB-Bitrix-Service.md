SB\Bitrix\Service
===============

Class Service




* Class name: Service
* Namespace: SB\Bitrix
* This is an **abstract** class
* This class implements: [SB\Interfaces\SB](SB-Interfaces-SB.md)






Methods
-------


### __construct

    mixed SB\Bitrix\Service::__construct()





* Visibility: **protected**




### loadModules

    mixed SB\Bitrix\Service::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Service::getSiteId()





* Visibility: **public**



