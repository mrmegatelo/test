SB\Exception\ForClass\ForProperty
===============

Class ForProperty




* Class name: ForProperty
* Namespace: SB\Exception\ForClass
* Parent class: [SB\Exception\ForClass](SB-Exception-ForClass.md)





Properties
----------


### $propertyName

    protected string $propertyName = ''





* Visibility: **protected**


### $className

    protected string $className = ''





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Exception::__construct(string $message, integer $code, \SB\Interfaces\Throwable|null $previous)

Exception constructor.



* Visibility: **public**
* This method is defined by [SB\Exception](SB-Exception.md)


#### Arguments
* $message **string**
* $code **integer**
* $previous **[SB\Interfaces\Throwable](SB-Interfaces-Throwable.md)|null**


