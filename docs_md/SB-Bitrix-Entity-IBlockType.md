SB\Bitrix\Entity\IBlockType
===============

Сущность &quot;Тип инфоблока&quot;
Содержит общие свойства и методы




* Class name: IBlockType
* Namespace: SB\Bitrix\Entity
* Parent class: [SB\Bitrix\Entity](SB-Bitrix-Entity.md)





Properties
----------


### $id

    public string $id

Идентификатор



* Visibility: **public**


### $name

    public string $name

Русское название



* Visibility: **public**


### $sort

    public integer $sort





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Bitrix\Entity::__construct()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Entity](SB-Bitrix-Entity.md)




### __get

    mixed SB\Bitrix\Entity\IBlockType::__get(string $name)





* Visibility: **public**


#### Arguments
* $name **string**



### __getInfo

    mixed SB\Bitrix\Entity\IBlockType::__getInfo()





* Visibility: **protected**




### getById

    \SB\Bitrix\Entity\IBlockType SB\Bitrix\Entity\IBlockType::getById(string $id)

Возвращает тип инфоблока по ID



* Visibility: **public**
* This method is **static**.


#### Arguments
* $id **string**



### getByFilter

    array<mixed,\SB\Bitrix\Entity\IBlockType> SB\Bitrix\Entity\IBlockType::getByFilter(array $arFilter)

Возаращает массив типов инфоблока



* Visibility: **public**
* This method is **static**.


#### Arguments
* $arFilter **array** - &lt;p&gt;, поля:
$arFilter[ID] - регистронезависимый по подстроке в коде типа
$arFilter[=ID]- точное совпадение с кодом типа
$arFilter[NAME] - регистронезависимый по подстроке в названии типа(для всех языков)
$arFilter[LANGUAGE_ID] - код языка&lt;/p&gt;



### create

    integer|boolean SB\Bitrix\Entity\IBlockType::create()

Создает тип инфоблока



* Visibility: **public**




### isExist

    boolean SB\Bitrix\Entity\IBlockType::isExist()

Проверяет существование



* Visibility: **public**




### loadModules

    mixed SB\Bitrix\Entity::loadModules(array $idList)

Bitrix констуктор класса



* Visibility: **protected**
* This method is defined by [SB\Bitrix\Entity](SB-Bitrix-Entity.md)


#### Arguments
* $idList **array**



### getSiteId

    mixed SB\Bitrix\Entity::getSiteId()





* Visibility: **public**
* This method is defined by [SB\Bitrix\Entity](SB-Bitrix-Entity.md)



