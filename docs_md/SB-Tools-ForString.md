SB\Tools\ForString
===============

Class ForString




* Class name: ForString
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)







Methods
-------


### getEndWord

    mixed SB\Tools\ForString::getEndWord(integer $count, array $arWords)

Возвращает окончание для слова в зависимости от количества



* Visibility: **public**
* This method is **static**.


#### Arguments
* $count **integer** - &lt;ul&gt;
&lt;li&gt;количество&lt;/li&gt;
&lt;/ul&gt;
* $arWords **array**


