SB\Handler\Ajax\Demo
===============

Class Handler
Базовый класс обработчиков




* Class name: Demo
* Namespace: SB\Handler\Ajax
* Parent class: [SB\Handler\Ajax](SB-Handler-Ajax.md)





Properties
----------


### $result

    protected \SB\Model\Response $result





* Visibility: **protected**


### $request

    protected \Bitrix\Main\HttpRequest $request





* Visibility: **protected**


### $params

    protected array $params





* Visibility: **protected**


### $method

    protected string $method





* Visibility: **protected**


Methods
-------


### write

    mixed SB\Handler\Ajax\Demo::write()





* Visibility: **public**




### __construct

    mixed SB\Handler\Ajax::__construct(\Bitrix\Main\HttpRequest $request, \SB\Model\Response $result)





* Visibility: **public**
* This method is defined by [SB\Handler\Ajax](SB-Handler-Ajax.md)


#### Arguments
* $request **Bitrix\Main\HttpRequest**
* $result **SB\Model\Response**



### getResult

    mixed SB\Handler\Ajax::getResult()





* Visibility: **public**
* This method is defined by [SB\Handler\Ajax](SB-Handler-Ajax.md)



