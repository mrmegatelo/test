SB\Entity\Item
===============






* Class name: Item
* Namespace: SB\Entity
* This is an **abstract** class





Properties
----------


### $deleteNotFound

    protected boolean $deleteNotFound = true

удалять ли не найденные в мапе элементы



* Visibility: **protected**


### $arData

    protected mixed $arData = array()





* Visibility: **protected**


### $child

    protected \ArrayIterator $child





* Visibility: **protected**


### $parent

    protected mixed $parent





* Visibility: **protected**


Methods
-------


### __debugInfo

    array SB\Entity\Item::__debugInfo()

убирает из вывода детей и родителей, чтобы не нагружать браузер



* Visibility: **public**




### __construct

    mixed SB\Entity\Item::__construct(array $arData, null $parent, boolean $createChild)

Item constructor.



* Visibility: **public**


#### Arguments
* $arData **array** - &lt;p&gt;массив преобразуемый в объект&lt;/p&gt;
* $parent **null** - &lt;p&gt;родитель данного элемента&lt;/p&gt;
* $createChild **boolean** - &lt;p&gt;нужно ли создавать детей&lt;/p&gt;



### checkParent

    boolean SB\Entity\Item::checkParent()

проверить существование родителя



* Visibility: **public**




### getParent

    null|object SB\Entity\Item::getParent()

получить родителя



* Visibility: **public**




### checkChild

    boolean SB\Entity\Item::checkChild($childKey)

проверить существование ребенка по ключу



* Visibility: **public**


#### Arguments
* $childKey **mixed**



### getChild

    mixed SB\Entity\Item::getChild($childKey)

получить ребенка по ключу



* Visibility: **public**


#### Arguments
* $childKey **mixed**



### getChildren

    \ArrayIterator SB\Entity\Item::getChildren()

получить всех детей



* Visibility: **public**




### getValueByPath

    boolean|mixed SB\Entity\Item::getValueByPath($path)

получить значение по путям ключей



* Visibility: **public**


#### Arguments
* $path **mixed**



### toArray

    array SB\Entity\Item::toArray()

преобразование объекта в массив



* Visibility: **public**




### __toArray

    mixed SB\Entity\Item::__toArray()





* Visibility: **public**




### getChildFunctions

    array SB\Entity\Item::getChildFunctions()

должен возвращать массив вида:
childKey => callbackFunction

в callback функцию передаются 2 парметра элемент по найденый по ключу childKey и объект родителя, для возможности
добавления его в конструктор для построения связи

* Visibility: **protected**
* This method is **abstract**.




### getMap

    array SB\Entity\Item::getMap()

получение карты объекта для объекта



* Visibility: **protected**
* This method is **abstract**.




### prepareData

    array SB\Entity\Item::prepareData($arData)

перебирает входной массив



* Visibility: **protected**


#### Arguments
* $arData **mixed**


