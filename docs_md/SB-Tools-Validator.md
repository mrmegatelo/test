SB\Tools\Validator
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Validator
* Namespace: SB\Tools
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $messages

    public mixed $messages





* Visibility: **public**
* This property is **static**.


Methods
-------


### init

    mixed SB\Tools\Validator::init()





* Visibility: **public**
* This method is **static**.




### email

    mixed SB\Tools\Validator::email($value)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $value **mixed**



### required

    mixed SB\Tools\Validator::required($value)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $value **mixed**



### fileExtention

    mixed SB\Tools\Validator::fileExtention($file, $arExtentions)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $file **mixed**
* $arExtentions **mixed**


