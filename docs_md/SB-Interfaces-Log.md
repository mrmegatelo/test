SB\Interfaces\Log
===============






* Interface name: Log
* Namespace: SB\Interfaces
* This is an **interface**






Methods
-------


### add

    mixed SB\Interfaces\Log::add($text)





* Visibility: **public**


#### Arguments
* $text **mixed**


