SB\Tools\chunkReadFilter
===============






* Class name: chunkReadFilter
* Namespace: SB\Tools
* This class implements: PHPExcel_Reader_IReadFilter




Properties
----------


### $_startRow

    private mixed $_startRow





* Visibility: **private**


### $_endRow

    private mixed $_endRow





* Visibility: **private**


Methods
-------


### setRows

    mixed SB\Tools\chunkReadFilter::setRows($startRow, $chunkSize)





* Visibility: **public**


#### Arguments
* $startRow **mixed**
* $chunkSize **mixed**



### readCell

    mixed SB\Tools\chunkReadFilter::readCell($column, $row, $worksheetName)





* Visibility: **public**


#### Arguments
* $column **mixed**
* $row **mixed**
* $worksheetName **mixed**


