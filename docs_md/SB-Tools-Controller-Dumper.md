SB\Tools\Controller\Dumper
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: Dumper
* Namespace: SB\Tools\Controller
* Parent class: [SB\Tools](SB-Tools.md)





Properties
----------


### $counter

    protected mixed $counter





* Visibility: **protected**


### $prev_time

    protected mixed $prev_time





* Visibility: **protected**


### $prev_time_simple

    protected mixed $prev_time_simple





* Visibility: **protected**
* This property is **static**.


### $i

    protected mixed $i = 1





* Visibility: **protected**
* This property is **static**.


### $memory

    protected mixed $memory





* Visibility: **protected**
* This property is **static**.


### $sEndOfMessageSymbol

    protected mixed $sEndOfMessageSymbol = PHP_EOL





* Visibility: **protected**
* This property is **static**.


Methods
-------


### __construct

    mixed SB\Tools\Controller\Dumper::__construct()





* Visibility: **private**




### __clone

    mixed SB\Tools\Controller\Dumper::__clone()





* Visibility: **private**




### d

    mixed SB\Tools\Controller\Dumper::d()

Dump



* Visibility: **public**
* This method is **static**.




### dd

    mixed SB\Tools\Controller\Dumper::dd()

Dump & Die



* Visibility: **public**
* This method is **static**.




### dJS

    mixed SB\Tools\Controller\Dumper::dJS($mData)

Дамп в консоль браузера. Обязательно должна быть подключена jQuery



* Visibility: **public**
* This method is **static**.


#### Arguments
* $mData **mixed**



### get_caller_info

    string SB\Tools\Controller\Dumper::get_caller_info(boolean $bFullFilePath)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $bFullFilePath **boolean**



### dump_caller_info

    mixed SB\Tools\Controller\Dumper::dump_caller_info($bFullFilePath)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $bFullFilePath **mixed**



### ts

    mixed SB\Tools\Controller\Dumper::ts(string $sSomeText)

Time simple (in milliseconds).

First call will start timer.

* Visibility: **public**
* This method is **static**.


#### Arguments
* $sSomeText **string**



### get_calling_method_name

    mixed SB\Tools\Controller\Dumper::get_calling_method_name()





* Visibility: **public**
* This method is **static**.




### memory_usage_show

    mixed SB\Tools\Controller\Dumper::memory_usage_show(string $sPreText)

Using:

_::MemoryUsageReset();
//
// Write code
//
_::MemoryUsageShow();
//
// Write code
//
_::MemoryUsageShow('Last call');

* Visibility: **public**
* This method is **static**.


#### Arguments
* $sPreText **string**



### memory_usage_reset

    mixed SB\Tools\Controller\Dumper::memory_usage_reset($sEndOfMessageSymbol)





* Visibility: **public**
* This method is **static**.


#### Arguments
* $sEndOfMessageSymbol **mixed**


