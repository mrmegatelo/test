SB\Service\Catalog
===============

Сервис API: Работа с каталогом




* Class name: Catalog
* Namespace: SB\Service





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getSectionsList

    array<mixed,\SB\Model\Section> SB\Service\Catalog::getSectionsList(boolean $useCache, boolean $clearCache)

Получение разделов каталога



* Visibility: **public**


#### Arguments
* $useCache **boolean**
* $clearCache **boolean**



### getSectionProductList

    array<mixed,\SB\Model\Product> SB\Service\Catalog::getSectionProductList(string $sectionCode, string $sortType, integer $countInPage, integer $pageNumber, boolean $useCache, boolean $clearCache)

Получение товаров раздела



* Visibility: **public**


#### Arguments
* $sectionCode **string** - &lt;ul&gt;
&lt;li&gt;символьный код раздела&lt;/li&gt;
&lt;/ul&gt;
* $sortType **string** - &lt;ul&gt;
&lt;li&gt;тип сортировки&lt;/li&gt;
&lt;/ul&gt;
* $countInPage **integer**
* $pageNumber **integer**
* $useCache **boolean**
* $clearCache **boolean**



### getProduct

    \SB\Model\Product SB\Service\Catalog::getProduct(string $productCode, string $storeCode)

Получение товара



* Visibility: **public**


#### Arguments
* $productCode **string** - &lt;ul&gt;
&lt;li&gt;символьный код товара&lt;/li&gt;
&lt;/ul&gt;
* $storeCode **string** - &lt;ul&gt;
&lt;li&gt;код аптеки&lt;/li&gt;
&lt;/ul&gt;



### getProductDescription

    \SB\Service\ProductDescription SB\Service\Catalog::getProductDescription(string $productCode)

Получение товара



* Visibility: **public**


#### Arguments
* $productCode **string** - &lt;ul&gt;
&lt;li&gt;символьный код товара&lt;/li&gt;
&lt;/ul&gt;



### searchProduct

    array<mixed,\SB\Model\Product> SB\Service\Catalog::searchProduct(string $searchQuery, string $searchType, string $sortType, boolean $useCache, boolean $clearCache)

Поиск товаров



* Visibility: **public**


#### Arguments
* $searchQuery **string** - &lt;ul&gt;
&lt;li&gt;строка запроса&lt;/li&gt;
&lt;/ul&gt;
* $searchType **string** - &lt;ul&gt;
&lt;li&gt;тип поиска&lt;/li&gt;
&lt;/ul&gt;
* $sortType **string** - &lt;ul&gt;
&lt;li&gt;тип сортировки&lt;/li&gt;
&lt;/ul&gt;
* $useCache **boolean**
* $clearCache **boolean**



### getInstance

    static SB\Service\Catalog::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Service\Catalog::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Service\Catalog::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Service\Catalog::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Service\Catalog::__clone()





* Visibility: **protected**



