SB\Model\Ajax\Error
===============






* Class name: Error
* Namespace: SB\Model\Ajax





Properties
----------


### $message

    public mixed $message





* Visibility: **public**


### $code

    public mixed $code





* Visibility: **public**


Methods
-------


### __construct

    mixed SB\Model\Ajax\Error::__construct($message, $code)





* Visibility: **public**


#### Arguments
* $message **mixed**
* $code **mixed**


