SB\Bitrix\Snippet\FilterType
===============

Class FilterType
Сниппеты Фильтра




* Class name: FilterType
* Namespace: SB\Bitrix\Snippet
* This is an **abstract** class
* Parent class: [SB\Snippet](SB-Snippet.md)



Constants
----------


### active

    const active = 'ACTIVE'





### activeIsYes

    const activeIsYes = 'Y'





### activeIsNo

    const activeIsNo = 'N'





### arActiveIsYes

    const arActiveIsYes = array(self::active => self::activeIsYes)





### arActiveIsNo

    const arActiveIsNo = array(self::active => self::activeIsNo)





### arActiveIsAll

    const arActiveIsAll = array(self::active => array(self::activeIsYes, self::activeIsNo))





### activeY

    const activeY = array(self::active => self::activeIsYes)





### activeN

    const activeN = array(self::active => self::activeIsNo)





### activeAll

    const activeAll = array(self::active => array(self::activeIsYes, self::activeIsNo))








