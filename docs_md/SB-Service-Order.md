SB\Service\Order
===============

Сервис API: Работа с заказами




* Class name: Order
* Namespace: SB\Service





Properties
----------


### $instance

    protected static $instance = null





* Visibility: **protected**
* This property is **static**.


Methods
-------


### getInstance

    static SB\Service\Order::getInstance(array $argument)

Создает и возвращает объект класса



* Visibility: **public**
* This method is **static**.


#### Arguments
* $argument **array**



### reset

    mixed SB\Service\Order::reset()





* Visibility: **public**
* This method is **static**.




### __construct

    mixed SB\Service\Order::__construct($argument)





* Visibility: **protected**


#### Arguments
* $argument **mixed**



### __wakeup

    mixed SB\Service\Order::__wakeup()





* Visibility: **protected**




### __clone

    mixed SB\Service\Order::__clone()





* Visibility: **protected**



