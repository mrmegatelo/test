SB\Tools\Output\BufferOutput
===============

Class Tools
Базовый класс утилит
Предназначен для хранения вспомогательных функций




* Class name: BufferOutput
* Namespace: SB\Tools\Output
* Parent class: [SB\Tools\Output](SB-Tools-Output.md)





Properties
----------


### $buffer

    protected mixed $buffer = ''





* Visibility: **protected**


Methods
-------


### __construct

    mixed SB\Tools\Output\BufferOutput::__construct()





* Visibility: **public**




### write

    mixed SB\Tools\Output::write($text)





* Visibility: **public**
* This method is **abstract**.
* This method is defined by [SB\Tools\Output](SB-Tools-Output.md)


#### Arguments
* $text **mixed**



### writeln

    mixed SB\Tools\Output::writeln($text)





* Visibility: **public**
* This method is **abstract**.
* This method is defined by [SB\Tools\Output](SB-Tools-Output.md)


#### Arguments
* $text **mixed**



### getBuffer

    mixed SB\Tools\Output\BufferOutput::getBuffer()





* Visibility: **public**



